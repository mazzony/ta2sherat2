<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/logout', [App\Http\Controllers\HomeController::class, 'logout'])->name('logout');
Route::get('/', function () {
    return view('home');
});
Route::get('/admin_login', function () {
    return view('admin_login');
});
Route::get('/sector/{id}', 'App\Http\Controllers\SectorController@single');
Route::get('/admin/{any}', 'App\Http\Controllers\AdminController@index')->where('any', '.*');
Route::get('/user/{any}', 'App\Http\Controllers\UserController@user_dashboad')->where('any', '.*');

// Route::get('/login_middle_page', 'App\Http\Controllers\Auth\loginController@login_middle_page');
// Route::get('/login_middle_page', 'App\Http\Controllers\Auth\loginController@login_middle_page');


Route::get('/login_middle_page', [App\Http\Controllers\Auth\LoginController::class, 'login_middle_page'])->name('login_middle_page');

Route::group(['middleware' => ['auth']], function () {
    Route::get('evaluators/print_ar/{id}','App\Http\Controllers\RequestController@print_ar');
});
