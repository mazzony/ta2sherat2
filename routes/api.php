<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::apiResource('sectors', 'App\Http\Controllers\SectorController');
Route::apiResource('users', 'App\Http\Controllers\UserController');
Route::get('sector/get_all', 'App\Http\Controllers\SectorController@getSectors');
Route::post('sector/get_all_fe', 'App\Http\Controllers\SectorController@getSectorsFe');
Route::get('sector/get_user_sector/{id}', 'App\Http\Controllers\SectorController@get_user_sectors');

Route::get('user/un_activated','App\Http\Controllers\UserController@getUnActivated');
Route::get('user/change_status/{id}','App\Http\Controllers\UserController@change_status');

Route::get('admin/list_admins','App\Http\Controllers\UserController@list_admins');
Route::get('admin/list_evaluators','App\Http\Controllers\UserController@list_evaluators');
Route::get('admin/list_super_evaluators','App\Http\Controllers\UserController@list_super_evaluators');

Route::get('admin/get_admin/{id}','App\Http\Controllers\UserController@get_admin');

Route::post('admin/add','App\Http\Controllers\UserController@add_admin');
Route::post('evaluator/add','App\Http\Controllers\UserController@add_evaluator');
Route::post('superevaluator/add','App\Http\Controllers\UserController@add_superevaluator');
Route::post('admin/update/{id}','App\Http\Controllers\UserController@update_admin');
Route::delete('admin/{id}', 'App\Http\Controllers\UserController@destroy');

Route::get('user/get_type/','App\Http\Controllers\UserController@get_type');
Route::get('user/get_user/','App\Http\Controllers\UserController@get_current_user');
Route::get('user/get_profile','App\Http\Controllers\UserController@get_profile');
Route::get('user/get_id/','App\Http\Controllers\UserController@get_user_id');
Route::get('user/get_user_sector/','App\Http\Controllers\UserController@get_user_sector');


Route::get('admin/list_factories','App\Http\Controllers\UserController@list_factories');
Route::get('admin/list_quarries','App\Http\Controllers\UserController@list_quarries');
Route::get('admin/list_traders','App\Http\Controllers\UserController@list_traders');
Route::get('sectors/view/{id}','App\Http\Controllers\SectorController@view');

Route::post('order/add/{id}','App\Http\Controllers\OrderController@add');
Route::get('order/my_orders/{id}','App\Http\Controllers\OrderController@my_orders');
Route::get('order/sector_orders/{id}','App\Http\Controllers\OrderController@sector_orders');
Route::get('order/get_order/{id}','App\Http\Controllers\OrderController@get_order');
Route::get('admin/order/all','App\Http\Controllers\OrderController@adminViewOrders');





Route::post('requests/add','App\Http\Controllers\RequestController@add');
Route::get('requests/delete_request/{id}','App\Http\Controllers\RequestController@delete_request');
Route::get('requests/my_requests/{id}','App\Http\Controllers\RequestController@my_requests');
Route::get('requests/order_requests/{id}','App\Http\Controllers\RequestController@order_requests');
Route::get('requests/get_request/{id}','App\Http\Controllers\RequestController@get_request');
Route::get('requests/trader_get_request/{id}','App\Http\Controllers\RequestController@trader_get_request');
Route::get('requests/admin_get_request/{id}','App\Http\Controllers\RequestController@admin_get_request');
Route::post('requests/accept/{id}','App\Http\Controllers\RequestController@set_processed');
Route::post('requests/refuse/{id}','App\Http\Controllers\RequestController@set_refused');
Route::get('requests/admin_accept/{id}','App\Http\Controllers\RequestController@admin_accept');
Route::get('requests/admin_refuse/{id}','App\Http\Controllers\RequestController@admin_refuse');
Route::get('admin/requests/all/{type}','App\Http\Controllers\RequestController@adminViewRequests');
Route::get('admin/admin_requests/{id}','App\Http\Controllers\RequestController@admin_requests');
Route::get('evaluator/listRequests','App\Http\Controllers\RequestController@listRequests');
Route::get('evaluators/get_request/{id}','App\Http\Controllers\RequestController@get_request');
Route::get('evaluators/set_processing/{id}','App\Http\Controllers\RequestController@set_processing');
Route::post('evaluators/set_approved/{id}','App\Http\Controllers\RequestController@set_approved');
Route::post('evaluators/set_refused/{id}','App\Http\Controllers\RequestController@set_refused');
Route::post('evaluators/set_final/{id}','App\Http\Controllers\RequestController@set_final');
Route::post('evaluators/return_to_eval/{id}','App\Http\Controllers\RequestController@return_to_eval');
Route::post('evaluators/set_returned/{id}','App\Http\Controllers\RequestController@set_returned');
// Route::get('evaluators/print_ar/{id}','App\Http\Controllers\RequestController@print_ar');
Route::get('exportStatistics','App\Http\Controllers\RequestController@exportStatistics');

Route::get('add_cities','App\Http\Controllers\CityController@add_cities');
Route::get('get_cities','App\Http\Controllers\CityController@get_cities');





Route::get('user/listRequests','App\Http\Controllers\RequestController@userListRequests');
Route::get('user/get_request/{id}','App\Http\Controllers\RequestController@get_request');
Route::post('requests/update/{id}','App\Http\Controllers\RequestController@update_request');

Route::get('user/my_requests_count','App\Http\Controllers\RequestController@my_requests_count');




Route::get('sendMail','App\Http\Controllers\RequestController@sendMail');

Route::get('set_locale/{lang}','App\Http\Controllers\LanguageController@set_locale');
Route::post('get_conditions','App\Http\Controllers\LanguageController@get_conditions');

Route::get('getUserDetails','App\Http\Controllers\SourceController@get_user_details');
Route::get('get_activities','App\Http\Controllers\SourceController@get_activities');

Route::get('get_notifications','App\Http\Controllers\NotificationController@get_all');
Route::get('seen/{id}','App\Http\Controllers\NotificationController@seen');


Route::get ('delete_requestt/{id}','App\Http\Controllers\RequestController@delete_requestt');





