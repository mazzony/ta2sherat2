-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 19, 2021 at 09:42 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";



/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ta2shera`
--

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(20) UNSIGNED NOT NULL PRIMARY KEY, 
  `destination_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seen` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `destination_id`, `title`, `body`, `seen`, `type`, `url`, `post_id`, `created_at`, `updated_at`) VALUES
(1, 2, 'Request refused', 'Yor Request8refused', 0, 'request_status', '8', 8, '2021-08-12 12:16:59', '2021-08-12 12:16:59');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL PRIMARY KEY,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sector_id` int(10) UNSIGNED NOT NULL,
  `trader_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(10) UNSIGNED NOT NULL PRIMARY KEY,
  `cr` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_symbol` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middleName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from` int(10) UNSIGNED NOT NULL,
  `response` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `responser` int(10) UNSIGNED NOT NULL,
  `response_comments` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachments` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `cr`, `ir`, `factory_name`, `factory_symbol`, `firstName`, `middleName`, `lastName`, `phone`, `email`, `details`, `status`, `from`, `response`, `responser`, `response_comments`, `attachments`, `created_at`, `updated_at`) VALUES
(1, 'nsnnn22', 'nnnn', 'njjjj', 'jjjjj', 'jjjjjbbh', 'bbb', 'bhb', '01122646495', 'myehia@fpconsultancy.com', 'a:2:{i:0;a:5:{s:12:\"employeeName\";s:3:\"kjj\";s:10:\"passNumber\";s:5:\"jjjdj\";s:10:\"visaNumber\";s:5:\"44558\";s:4:\"from\";s:10:\"2021-08-26\";s:2:\"to\";s:10:\"2021-09-11\";}i:1;a:5:{s:12:\"employeeName\";s:5:\"99999\";s:10:\"passNumber\";s:2:\"oo\";s:10:\"visaNumber\";s:3:\"222', 'new', 2, '', 0, '', '', '2021-08-10 12:02:10', '2021-08-10 12:02:10'),
(2, 'nsnnn22', 'nnnn', 'njjjj', 'jjjjj', 'jjjjjbbh', 'bbb', 'bhb', '01122646495', 'myehia@fpconsultancy.com', 'a:2:{i:0;a:5:{s:12:\"employeeName\";s:3:\"kjj\";s:10:\"passNumber\";s:5:\"jjjdj\";s:10:\"visaNumber\";s:5:\"44558\";s:4:\"from\";s:10:\"2021-08-26\";s:2:\"to\";s:10:\"2021-09-11\";}i:1;a:5:{s:12:\"employeeName\";s:5:\"99999\";s:10:\"passNumber\";s:2:\"oo\";s:10:\"visaNumber\";s:3:\"222', 'new', 2, '', 0, '', '', '2021-08-10 12:06:28', '2021-08-10 12:06:28'),
(3, '45588', '225588', 'Mazz', 'ssss', 'مازن', 'يحيى', 'عمر', '01122646495', 'mazzony13@outlook.sa', 'a:2:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:4:\"2222\";s:10:\"passNumber\";s:3:\"xjs\";s:10:\"visaNumber\";s:3:\"223\";s:4:\"from\";s:4:\"Giza\";s:2:\"to\";s:3:\"KSA\";}i:1;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:4:\"5855\";s:10:\"passNumber\";s:4:\"kksk\";s:10:\"visaNumber\";s:5:\"88888\";s:4:\"from\";s:2:\"LL\";s:2:\"to\";s:2:\"MM\";}}', 'returned', 3, '', 0, '', '', '2021-08-11 09:33:51', '2021-08-15 10:56:34'),
(4, 'Tetst', 'ttttst', 'ttstst', 'tttt', 'Mazen', 'Yehia', 'Omar', '85554', 'mazzony13@outlook.sa', 's:31:\"[object Object],[object Object]\";', '1628690346.pdf', 2, '', 0, '', '', '2021-08-11 11:59:06', '2021-08-11 11:59:06'),
(5, '2344', '775555', 'k', 'kjjj', 'Mazen', 'Yehia', 'Omar', '01122646495', 'mazzony13@outlook.sa', 's:206:\"[{\"employeeName\":\"234\",\"passNumber\":\"mdjdjdj\",\"visaNumber\":\"3333\",\"from\":\"2021-08-26\",\"to\":\"2021-08-27\"},{\"employeeName\":\"777\",\"passNumber\":\"0099\",\"visaNumber\":\"0099\",\"from\":\"2021-08-06\",\"to\":\"2021-09-11\"}]\";', '1628690624.pdf', 2, '', 0, '', '', '2021-08-11 12:03:44', '2021-08-11 12:03:44'),
(6, '2344', '775555', 'k', 'kjjj', 'Mazen', 'Yehia', 'Omar', '01122646495', 'mazzony13@outlook.sa', 'a:2:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:3:\"234\";s:10:\"passNumber\";s:7:\"mdjdjdj\";s:10:\"visaNumber\";s:4:\"3333\";s:4:\"from\";s:10:\"2021-08-26\";s:2:\"to\";s:10:\"2021-08-27\";}i:1;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:3:\"777\";s:10:\"passNumber\";s:4:\"0099\"', '1628691160.pdf', 2, '', 0, '', '', '2021-08-11 12:12:40', '2021-08-11 12:12:40'),
(7, '2344', '775555', 'k', 'kjjj', 'Mazen', 'Yehia', 'Omar', '01122646495', 'mazzony13@outlook.sa', 'a:2:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:3:\"234\";s:10:\"passNumber\";s:7:\"mdjdjdj\";s:10:\"visaNumber\";s:4:\"3333\";s:4:\"from\";s:10:\"2021-08-26\";s:2:\"to\";s:10:\"2021-08-27\";}i:1;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:3:\"777\";s:10:\"passNumber\";s:4:\"0099\";s:10:\"visaNumber\";s:4:\"0099\";s:4:\"from\";s:10:\"2021-08-06\";s:2:\"to\";s:10:\"2021-09-11\";}}', 'processing', 2, '', 0, '', '', '2021-08-11 12:14:57', '2021-08-11 12:21:59'),
(8, '2344', '775555', 'k', 'kjjj', 'Mazen', 'Yehia', 'Omar', '01122646495', 'mazzony13@outlook.sa', 'a:2:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:3:\"234\";s:10:\"passNumber\";s:7:\"mdjdjdj\";s:10:\"visaNumber\";s:4:\"3333\";s:4:\"from\";s:10:\"2021-08-26\";s:2:\"to\";s:10:\"2021-08-27\";}i:1;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:3:\"777\";s:10:\"passNumber\";s:4:\"0099\";s:10:\"visaNumber\";s:4:\"0099\";s:4:\"from\";s:10:\"2021-08-06\";s:2:\"to\";s:10:\"2021-09-11\";}}', 'processing', 2, '', 0, '', '1628691895.pdf', '2021-08-11 12:24:55', '2021-08-12 12:17:00'),
(9, '2344', '775555', 'k', 'kjjj', 'Mazen', 'Yehia', 'Omar', '01122646495', 'mazzony13@outlook.sa', 'a:2:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:3:\"234\";s:10:\"passNumber\";s:7:\"mdjdjdj\";s:10:\"visaNumber\";s:4:\"3333\";s:4:\"from\";s:10:\"2021-08-26\";s:2:\"to\";s:10:\"2021-08-27\";}i:1;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:3:\"777\";s:10:\"passNumber\";s:4:\"0099\";s:10:\"visaNumber\";s:4:\"0099\";s:4:\"from\";s:10:\"2021-08-06\";s:2:\"to\";s:10:\"2021-09-11\";}}', 'processed', 2, '', 4, '', '1628778276.pdf', '2021-08-12 12:24:36', '2021-08-15 06:45:46'),
(10, '2344', '775555', 'k', 'kjjj', 'Mazen', 'Yehia', 'Omar', '01122646495', 'mazzony13@outlook.sa', 'a:2:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:3:\"234\";s:10:\"passNumber\";s:7:\"mdjdjdj\";s:10:\"visaNumber\";s:4:\"3333\";s:4:\"from\";s:10:\"2021-08-26\";s:2:\"to\";s:10:\"2021-08-27\";}i:1;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:3:\"777\";s:10:\"passNumber\";s:4:\"0099\";s:10:\"visaNumber\";s:4:\"0099\";s:4:\"from\";s:10:\"2021-08-06\";s:2:\"to\";s:10:\"2021-09-11\";}}', 'processed', 2, '', 4, 'ssss', '1628787632.pdf', '2021-08-12 15:00:32', '2021-08-15 06:54:14'),
(11, '2344', '775555', 'k', 'kjjj', 'Mazen', 'Yehia', 'Omar', '01122646495', 'mazzony13@outlook.sa', 'a:2:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:3:\"234\";s:10:\"passNumber\";s:7:\"mdjdjdj\";s:10:\"visaNumber\";s:4:\"3333\";s:4:\"from\";s:10:\"2021-08-26\";s:2:\"to\";s:10:\"2021-08-27\";}i:1;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:3:\"777\";s:10:\"passNumber\";s:4:\"0099\";s:10:\"visaNumber\";s:4:\"0099\";s:4:\"from\";s:10:\"2021-08-06\";s:2:\"to\";s:10:\"2021-09-11\";}}', 'processed', 2, '', 4, '22222', '1628787637.pdf', '2021-08-12 15:00:37', '2021-08-15 07:00:19'),
(12, '2344', '775555', 'k', 'kjjj', 'Mazen', 'Yehia', 'Omar', '01122646495', 'mazzony13@outlook.sa', 'a:2:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:3:\"234\";s:10:\"passNumber\";s:7:\"mdjdjdj\";s:10:\"visaNumber\";s:4:\"3333\";s:4:\"from\";s:10:\"2021-08-26\";s:2:\"to\";s:10:\"2021-08-27\";}i:1;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:3:\"777\";s:10:\"passNumber\";s:4:\"0099\";s:10:\"visaNumber\";s:4:\"0099\";s:4:\"from\";s:10:\"2021-08-06\";s:2:\"to\";s:10:\"2021-09-11\";}}', 'processed', 2, '', 4, '', '1628787724.pdf', '2021-08-12 15:02:04', '2021-08-15 07:18:03'),
(13, '64738', '123455', 'Mazzzz', 'maz', 'مازن', 'يحيى', 'عمر', '01122646495', 'mazzony13@gmail.com', 'a:1:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:4:\"shsj\";s:10:\"passNumber\";s:5:\"jsjjs\";s:10:\"visaNumber\";s:3:\"332\";s:4:\"from\";s:10:\"2021-08-12\";s:2:\"to\";s:10:\"2021-09-11\";}}', 'processed', 3, '', 4, 'ssss', '1628953573.pdf', '2021-08-14 13:06:13', '2021-08-15 07:19:46'),
(14, '64738', '123455', 'Mazzzz', 'maz', 'مازن', 'يحيى', 'عمر', '01122646495', 'mazzony13@gmail.com', 'a:1:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:4:\"shsj\";s:10:\"passNumber\";s:5:\"jsjjs\";s:10:\"visaNumber\";s:3:\"332\";s:4:\"from\";s:10:\"2021-08-12\";s:2:\"to\";s:10:\"2021-09-11\";}}', 'processed', 3, '', 4, 'SSS', '1628953606.pdf', '2021-08-14 13:06:46', '2021-08-15 07:33:05'),
(15, '64738', '123455', 'Mazzzz', 'maz', 'مازن', 'يحيى', 'عمر', '01122646495', 'mazzony13@gmail.com', 'a:1:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:4:\"shsj\";s:10:\"passNumber\";s:5:\"jsjjs\";s:10:\"visaNumber\";s:3:\"332\";s:4:\"from\";s:10:\"2021-08-12\";s:2:\"to\";s:10:\"2021-09-11\";}}', 'processed', 4, '', 4, 'WW', '1629019158.pdf', '2021-08-15 07:19:18', '2021-08-15 07:33:55'),
(16, '64738', '123455', 'Mazzzz', 'maz', 'مازن', 'يحيى', 'عمر', '01122646495', 'mazzony13@gmail.com', 'a:1:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:4:\"shsj\";s:10:\"passNumber\";s:5:\"jsjjs\";s:10:\"visaNumber\";s:3:\"332\";s:4:\"from\";s:10:\"2021-08-12\";s:2:\"to\";s:10:\"2021-09-11\";}}', 'processed', 4, '', 4, '', '1629024826.pdf', '2021-08-15 08:53:46', '2021-08-15 08:55:30'),
(17, '64738', '123455', 'Mazzzz', 'maz', 'مازن', 'يحيى', 'عمر', '01122646495', 'mazzony13@gmail.com', 'a:1:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:4:\"shsj\";s:10:\"passNumber\";s:5:\"jsjjs\";s:10:\"visaNumber\";s:3:\"332\";s:4:\"from\";s:10:\"2021-08-12\";s:2:\"to\";s:10:\"2021-09-11\";}}', 'returned', 4, '', 4, '2222', '1629026051.pdf', '2021-08-15 09:14:11', '2021-08-15 10:15:03'),
(18, '45588', '225588', 'Mazz', 'ssss', 'مازن', 'يحيى', 'عمر', '01122646495', 'mazzony13@outlook.sa', 'a:2:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:4:\"2222\";s:10:\"passNumber\";s:3:\"xjs\";s:10:\"visaNumber\";s:3:\"223\";s:4:\"from\";s:4:\"Giza\";s:2:\"to\";s:3:\"KSA\";}i:1;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:4:\"5855\";s:10:\"passNumber\";s:4:\"kksk\";s:10:\"visaNumber\";s:5:\"88888\";s:4:\"from\";s:2:\"LL\";s:2:\"to\";s:2:\"MM\";}}', 'returned', 3, '', 4, 'ssss', '1629030555.pdf', '2021-08-15 10:29:15', '2021-08-15 10:55:32'),
(19, '52258', '3369', 'مصنع 1', '2255', 'مازن', 'يحيى', 'عمر', '01122646495', 'mazzony13@outlook.sa', 'a:2:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:8:\"مازن\";s:10:\"passNumber\";s:7:\"2255689\";s:10:\"visaNumber\";s:4:\"9985\";s:4:\"from\";s:6:\"مصر\";s:2:\"to\";s:14:\"السعوية\";}i:1;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:10:\"مازن 2\";s:10:\"passNumber\";s:5:\"65885\";s:10:\"visaNumber\";s:5:\"22225\";s:4:\"from\";s:13:\"سعودية \";s:2:\"to\";s:6:\"مصر\";}}', 'processed', 3, '', 4, '', '1629033237.pdf', '2021-08-15 11:13:57', '2021-08-15 12:58:18'),
(20, '37788', '12334556', 'مصنع مازن', 'MM', 'مازن', 'يحيى', 'عمر', '01122646495', 'mazzony13@outlook.sa', 'a:2:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:9:\"مازن \";s:10:\"passNumber\";s:6:\"555885\";s:10:\"visaNumber\";s:4:\"5558\";s:4:\"from\";s:6:\"مصر\";s:2:\"to\";s:16:\"السعودية\";}i:1;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:6:\"علي\";s:10:\"passNumber\";s:4:\"8555\";s:10:\"visaNumber\";s:4:\"5588\";s:4:\"from\";s:8:\"عمان\";s:2:\"to\";s:16:\"السعودية\";}}', 'processed', 3, '', 4, '', '1629041252.pdf', '2021-08-15 13:27:32', '2021-08-16 09:32:57'),
(21, '131313', '2244', 'مصنع 22', 'م22', 'مازن', 'يحيى', 'عمر', '01122646495', 'mazzony13@outlook.sa', 'a:2:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:8:\"مازن\";s:10:\"passNumber\";s:5:\"22258\";s:10:\"visaNumber\";s:5:\"22588\";s:4:\"from\";s:6:\"مصر\";s:2:\"to\";s:16:\"السعودية\";}i:1;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:8:\"محمد\";s:10:\"passNumber\";s:5:\"99558\";s:10:\"visaNumber\";s:3:\"558\";s:4:\"from\";s:16:\"السعودية\";s:2:\"to\";s:6:\"مصر\";}}', 'processed', 3, '', 4, '', '1629192994.docx', '2021-08-17 07:36:34', '2021-08-17 07:40:28'),
(22, '7635535', '23848', 'مصنع33', '33', 'مازن', 'يحيى', 'عمر', '01122646495', 'mazzony13@outlook.sa', 'a:1:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:10:\"محمود\";s:10:\"passNumber\";s:4:\"2344\";s:10:\"visaNumber\";s:4:\"1133\";s:4:\"from\";s:6:\"مصر\";s:2:\"to\";s:6:\"جدة\";}}', 'processed', 3, '', 4, '', '', '2021-08-17 07:45:02', '2021-08-17 07:46:14'),
(23, '222', '11111', 'ssss', 'sssss', 'مازن', 'يحيى', 'عمر', '01122646495', 'mazzony13@outlook.sa', 'a:1:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:4:\"1244\";s:10:\"passNumber\";s:3:\"765\";s:10:\"visaNumber\";s:3:\"666\";s:4:\"from\";s:3:\"lll\";s:2:\"to\";s:3:\"ggg\";}}', 'new', 3, '', 0, '', '', '2021-08-18 13:04:42', '2021-08-18 13:04:42'),
(24, '222', '11111', 'ssss', 'sssss', 'مازن', 'يحيى', 'عمر', '01122646495', 'mazzony13@outlook.sa', 'a:1:{i:0;O:8:\"stdClass\":5:{s:12:\"employeeName\";s:4:\"1244\";s:10:\"passNumber\";s:3:\"765\";s:10:\"visaNumber\";s:3:\"666\";s:4:\"from\";s:3:\"lll\";s:2:\"to\";s:3:\"ggg\";}}', 'processed', 3, '', 4, '', '', '2021-08-18 13:05:44', '2021-08-18 13:10:24');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL PRIMARY KEY,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'user', 'A regular user', '2021-08-10 09:01:05', '2021-08-10 09:01:05'),
(2, 'admin', 'An admin user', '2021-08-10 09:01:05', '2021-08-10 09:01:05'),
(3, 'evaluator', 'Evaluator', '2021-08-10 09:01:05', '2021-08-10 09:01:05');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL PRIMARY KEY,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 1, 3),
(4, 3, 4);

-- --------------------------------------------------------

--
-- Table structure for table `sectors`
--

CREATE TABLE `sectors` (
  `id` int(10) UNSIGNED NOT NULL PRIMARY KEY,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sectors`
--

INSERT INTO `sectors` (`id`, `name`, `description`, `region`, `featured_image`, `parent`, `created_at`, `updated_at`) VALUES
(1, 'Factories', 'Factories Sector is here ', 'Factories Region', '', 0, '2021-08-10 09:01:05', '2021-08-10 09:01:05'),
(2, 'Quarries', 'Quarries Sector is here ', 'Quarries Region', '', 0, '2021-08-10 09:01:05', '2021-08-10 09:01:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL PRIMARY KEY UNIQUE KEY,
  `firstName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middleName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `national_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstName`, `middleName`, `lastName`, `email`, `email_verified_at`, `password`, `status`, `type`, `phone`, `national_number`, `address`, `cr`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mazen', 'Yehia', 'Omar', 'mazzony13@gmil.com', NULL, '$2y$10$pNzQjD0.O8CZsmks9Dn3Uu2Fcb4/w5FH22mop0/Rv62t5Wy2CLfRy', 'active', 'system_user', NULL, NULL, NULL, NULL, NULL, '2021-08-10 09:01:05', '2021-08-10 09:01:05'),
(2, 'Mazen', 'Yehia', 'Omar', 'mazzony13@outlook.saa', NULL, '$2y$10$mdlfhBHg3hCwQqiGWexWMeCxQXivTMaOWiMUQdhIRqf867bfCpa.2', 'active', 'system_user', NULL, NULL, NULL, NULL, NULL, '2021-08-10 09:01:05', '2021-08-10 09:01:05'),
(3, 'مازن', 'يحيى', 'عمر', 'mazzony13@outlook.sa', NULL, '$2y$10$tVJNCz5GDc/YYNVIDBMSAeToRGUyUoWs8vAzJUi8IOxOdk85r32LC', 'active', 'user', '01122646495', NULL, NULL, NULL, NULL, '2021-08-14 12:40:28', '2021-08-14 12:40:28'),
(4, 'فارس', 'يحيى', 'سعيد', 'fares@gmail.com', NULL, '$2y$10$Of6LckFciU/zJvvFLnkSG.prdxbuNNuUR1zHrESe2dZhOOVqO/iay', 'active', 'evaluator', NULL, NULL, NULL, NULL, NULL, '2021-08-14 13:29:46', '2021-08-14 13:29:46'),
(5, 'مازن', NULL, NULL, NULL, NULL, NULL, 'active', 'user', NULL, NULL, NULL, NULL, NULL, '2021-08-16 18:22:22', '2021-08-16 18:22:22'),
(6, 'مازن', NULL, NULL, NULL, NULL, NULL, 'active', 'user', NULL, '247000177302', NULL, NULL, NULL, '2021-08-16 18:23:28', '2021-08-16 18:23:28'),
(7, 'مازن', NULL, NULL, '99833', NULL, '$2y$10$NoFsfFD.xKEpS7ZaEE0tTe314OfvxhMSrOKBxioHbrEEQ2h7zoT4a', 'active', 'user', NULL, '99833', NULL, NULL, NULL, '2021-08-16 19:51:19', '2021-08-16 19:51:19'),
(8, 'مازن', NULL, NULL, '101010', NULL, '$2y$10$simOnP.bByoM3sOvaOmdAu2Lx8D0oPzeGLEjWT4aVXlI3rlRRgYxO', 'active', 'user', NULL, '101010', NULL, NULL, NULL, '2021-08-16 19:52:02', '2021-08-16 19:52:02'),
(9, 'مازن', NULL, NULL, '333333', NULL, '$2y$10$xPf1wmeId8s6vosNyqtC3..5r7I8kkn5U47cYEOHfpx6Q35xEnfJO', 'active', 'user', NULL, '333333', NULL, NULL, NULL, '2021-08-16 19:59:25', '2021-08-16 19:59:25'),
(10, 'مازن', NULL, NULL, 'kekkkkkk``', NULL, '$2y$10$asfk2sSUrLcBlP0Ya29dfemKK3Dq02CXMKB2uD2rI9J6e5uwondAG', 'active', 'user', NULL, 'kekkkkkk``', NULL, NULL, NULL, '2021-08-16 20:01:00', '2021-08-16 20:01:00'),
(11, 'مازن', NULL, NULL, 'kekkkkkksss``', NULL, '$2y$10$KDGKkul8zKzi1HgQ5ehUNefFPmH9S8z7rk4x.tfgQ6jXtpWgHLSQW', 'active', 'user', NULL, 'kekkkkkksss``', NULL, NULL, NULL, '2021-08-16 20:01:45', '2021-08-16 20:01:45'),
(12, 'مازن', NULL, NULL, 'kekkkkkksssss``', NULL, '$2y$10$MPjtTzI167KtNs1TiBsocuWNjBfuZBQJAhAgsmMkgBC9EMmNuGuZ2', 'active', 'user', NULL, 'kekkkkkksssss``', NULL, NULL, NULL, '2021-08-16 20:02:17', '2021-08-16 20:02:17'),
(13, 'مازن', NULL, NULL, 'kekkkkksssksssss``', NULL, '$2y$10$h/O79HcgsiGUK.68ur8r..OSbQ8rjOkJJc.5J8BRyDTsw6QqcjkMC', 'active', 'user', NULL, 'kekkkkksssksssss``', NULL, NULL, NULL, '2021-08-16 20:05:25', '2021-08-16 20:05:25'),
(14, 'مازن', NULL, NULL, '12', NULL, '$2y$10$VaKO0XHM6DQgXJVEI/Nm7uxWtso7lKmBt44a8c/JSAFewSuecuPqK', 'active', 'user', NULL, '12', NULL, NULL, NULL, '2021-08-16 20:06:21', '2021-08-16 20:06:21'),
(15, 'مازن', NULL, NULL, '122', NULL, '$2y$10$px7MjyHfaP/TJHKBf5N19OwY8xzWZQsiSo5yEzcT5zGIfrC34ASZu', 'active', 'user', NULL, '122', NULL, NULL, NULL, '2021-08-16 20:09:02', '2021-08-16 20:09:02'),
(16, 'مازن', NULL, NULL, '123', NULL, '$2y$10$c8vJNHJaBmX9dEuwL7uk.O.B/zJU0cW7zHtSwCvS0Jnon6hu.34Ma', 'active', 'user', NULL, '123', NULL, NULL, NULL, '2021-08-16 20:09:43', '2021-08-16 20:09:43'),
(17, 'مازن', NULL, NULL, '131313', NULL, '$2y$10$LyTSocBP/uqDqlTCN00rH.0y2kbkQMnTubWoLSRQFvdPZUFT9OpDe', 'active', 'user', NULL, '131313', NULL, NULL, NULL, '2021-08-16 20:10:24', '2021-08-16 20:10:24'),
(18, 'مازن', NULL, NULL, '8080', NULL, '$2y$10$KSDymXhskaPEIxTG1rVaJex0YFtqIy5pFMz/rujeHqGm1YtKS6WFa', 'active', 'user', NULL, '8080', NULL, NULL, NULL, '2021-08-16 20:10:36', '2021-08-16 20:10:36'),
(19, 'Mazen', NULL, NULL, '9090', NULL, '$2y$10$JMCBhF99FIxVl0IZKaTP/.j.doXjOlvo88dSzolY4FA4Y9kyUN9Om', 'active', 'user', NULL, '9090', NULL, NULL, NULL, '2021-08-16 20:15:22', '2021-08-16 20:15:22'),
(20, 'مازن', NULL, NULL, '5050', NULL, '$2y$10$J63N4dxxq5ieIUnDjpuVvOQnGKDGdSA.2ft1pycghQyZ72Jpq9v/e', 'active', 'user', NULL, '5050', NULL, NULL, NULL, '2021-08-16 20:23:54', '2021-08-16 20:23:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `notifications`
--


--
-- Indexes for table `roles`
--

--
-- Indexes for table `sectors`
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sectors`
--
ALTER TABLE `sectors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
