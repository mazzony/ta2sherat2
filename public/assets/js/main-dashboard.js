/*==================================================
			Counter - up of card
==================================================*/
$(function () {
    $('.counter-card').counterUp({
        delay: 8,
        time: 1000
    });
    $('.card').matchHeight(); // set Same Height For Work Cards
    $('.chat_card').matchHeight(); // set Same Height For Work Cards
    //$('.selectpicker').selectpicker(); //Select library
    $('select').selectric(); //select-lib

});
/*==================================================
	toggle show/hide password
==================================================*/
$('.show_password_btn').click(function () {
    $(this.firstChild).toggleClass('change-icon-show-password');
    let element = $(this.parentElement.nextElementSibling);
    if(element.attr('type') === 'password'){
        element.attr('type', 'text');
    }else {
        element.attr('type', 'password');
    }
});
/*==================================================
			Nice Scroll
==================================================*/
$(function() {
    var dir = $('html').attr('dir');
    if(dir === "ltr"){
        dir = "right";
    }else{
        dir = "left";
    }
    $("html").niceScroll({
        cursorcolor: "#302D43",
        cursorwidth: "6px",
        cursoropacitymin: 0.5,
        cursorborder: "0px solid #000",
        autohidemode: "scroll",
        enabletranslate3d: true,
        hidecursordelay: 400,
        cursorfixedheight: false,
        enablekeyboard: true,
        horizrailenabled: false,
        bouncescroll: false,
        smoothscroll: true,
        iframeautoresize: true,
        touchbehavior: false,
        railalign: dir,
        zindex: 1
    });
    $("html").getNiceScroll().resize();
    $("html").mouseover(function () {
        $("html").getNiceScroll().resize();
    });
    $('#menu-dashboard').niceScroll({
        cursorcolor:		"#FEE35F",
        cursorwidth: 		"6px",
        cursoropacitymin:   0.5,
        cursorborder: 		"0px solid #000",
        autohidemode: 		"scroll",
        enabletranslate3d: true,
        hidecursordelay: 	400,
        cursorfixedheight: 	false,
        enablekeyboard: 	true,
        horizrailenabled: 	false,
        bouncescroll: 		false,
        smoothscroll: 		true,
        iframeautoresize: 	true,
        touchbehavior: 		false,
        railalign: dir,
        zindex: 1
    });
    $("#menu-dashboard").getNiceScroll().resize();
    $("#menu-dashboard").mouseover(function () {
        $("#menu-dashboard").getNiceScroll().resize();
    });
});
/*==================================================
	side bar menu
==================================================*/
$(window).on('load',function () {
    setTimeout(function () {
        var ele_id = localStorage.getItem('id');
        var t_ele = $('#'+ele_id+'');
        var checkElement = t_ele.next();

        if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            $(t_ele).find('i:last-child').toggleClass('toggle-icon-side-bar-menu');
            //$(this).closest('li').removeClass('active-menu-dashboard');
            checkElement.slideUp('normal');
        }
        if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            $(t_ele).find('i:last-child').toggleClass('toggle-icon-side-bar-menu');
            //console.log(t_ele.find('i:last-child'));
            $('#side-bar-menu ul ul:visible').slideUp('normal');
            $('#side-bar-menu ul ul:visible').parent().find('ul li.active-sub-menu-dashboard').removeClass('active-sub-menu-dashboard active-menu-dashboard');
            $('#side-bar-menu ul ul:visible').parent().find('i.icon-left-side-bar').toggleClass('toggle-icon-side-bar-menu');
            checkElement.slideDown('normal');
        }
        if(t_ele.closest('li').find('ul').children().length == 0) {
            $('#side-bar-menu ul ul:visible').slideUp('normal');
            $('#side-bar-menu ul ul:visible').parent().find('ul li.active-sub-menu-dashboard').removeClass('active-sub-menu-dashboard active-menu-dashboard');
            $('#side-bar-menu ul ul:visible').parent().find('i.icon-left-side-bar').toggleClass('toggle-icon-side-bar-menu');
            return true;
        } else {
            return false;
        }
    },500);

});
$(function () {
    //show hide side bar menu
    $('#show-side-bar-menu').on('click', function () {
       $('#menu-dashboard').toggleClass('show-side-bar');
       $('header .navbar').toggleClass('hide-side-bar');
       $('main .content-dashboard').toggleClass('hide-side-bar');
    });

    //active link side bar
    $('#side-bar-menu ul li.side-bar-item:not(:last-child) a.no_have_ul').filter(function(){
            return this.href === location.href
    }).parent().addClass('active-menu-dashboard').siblings().removeClass('active-menu-dashboard');
    $('#side-bar-menu ul li.side-bar-item:not(:last-child) a.no_have_ul').click(function(){
        //alert($(this).attr('id'));
        localStorage.setItem('id', $(this).attr('id'));
        $(this).parent().addClass('active-menu-dashboard').siblings().removeClass('active-menu-dashboard');
    });

    $('#side-bar-menu ul li.side-bar-item:not(:last-child) a.have_ul').click(function(){
        //alert($(this).attr('id'));
        localStorage.setItem('id', $(this).attr('id'));
        $(this).parent().addClass('active-menu-dashboard').siblings().removeClass('active-menu-dashboard');
    });

    //active link side bar
    $('#side-bar-menu ul li.side-bar-item:not(:last-child) a.have_ul').filter(function(){
        return $(this).attr('id') === localStorage.getItem('id')
    }).parent().addClass('active-menu-dashboard').siblings().removeClass('active-menu-dashboard');

    //child of links side bar
    $('#side-bar-menu > ul > li > a').click(function() {
        var checkElement = $(this).next();
        if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            $(this.lastChild).toggleClass('toggle-icon-side-bar-menu');
            //$(this).closest('li').removeClass('active-menu-dashboard');
            checkElement.slideUp('normal');
        }
        if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            $(this.lastChild).toggleClass('toggle-icon-side-bar-menu');
            $('#side-bar-menu ul ul:visible').slideUp('normal');
            $('#side-bar-menu ul ul:visible').parent().find('ul li.active-sub-menu-dashboard').removeClass('active-sub-menu-dashboard active-menu-dashboard');
            $('#side-bar-menu ul ul:visible').parent().find('i.icon-left-side-bar').toggleClass('toggle-icon-side-bar-menu');
            checkElement.slideDown('normal');
        }
        if($(this).closest('li').find('ul').children().length == 0) {
            $('#side-bar-menu ul ul:visible').slideUp('normal');
            $('#side-bar-menu ul ul:visible').parent().find('ul li.active-sub-menu-dashboard').removeClass('active-sub-menu-dashboard active-menu-dashboard');
            $('#side-bar-menu ul ul:visible').parent().find('i.icon-left-side-bar').toggleClass('toggle-icon-side-bar-menu');
            return true;
        } else {
            return false;
        }
    });
    //active sub menu link side bar
    $('#side-bar-menu ul ul li a').filter(function(){
        return this.href === location.href
    }).parent().addClass('active-sub-menu-dashboard').siblings().removeClass('active-sub-menu-dashboard');
    $('#side-bar-menu ul ul li a').click(function(){
        /*$(this).parent().closest("ul").parent().find('>a').attr('href', $(this).attr('href'));
        let ele = $(this).parent().closest("ul").parent().find('>a');*/
        $(this).parent().addClass('active-sub-menu-dashboard').siblings().removeClass('active-sub-menu-dashboard');
    });
});
/*==================================================
	Loader
==================================================*/
$(window).on('load', function () {
    setTimeout(function () {
        $('#preloader-container').fadeOut('slow');
    },500);
});

$("a.smooth-scroll").click(function (event) {

    event.preventDefault();

    // get section id like #about, #servcies, #work, #team and etc.
    var section_id = $(this).attr("href");

    $("html, body").animate({
        scrollTop: $(section_id).offset().top - 90
    }, 1250, "easeInOutExpo");

});