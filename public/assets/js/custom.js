/*==================================================
	init library
==================================================*/
$(function () {
    new WOW().init(); //WoW library
    $('.project_card').matchHeight(); // set Same Height For Work Cards
    $('.card_home_cover_dark').matchHeight(); // set Same Height For Work Cards
    //$('.selectpicker').selectpicker(); //Select library
    $('.img_link_popup').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true,
        }
    });
    $('.custom_selected').selectric({
        maxHeight: 200,
        responsive: true
    }); //select-lib
});

/*==================================================
	toggle show/hide password
==================================================*/
$('.show_password_btn').click(function () {
    $(this.firstChild).toggleClass('change-icon-show-password');
    let element = $(this.parentElement.nextElementSibling);
    if(element.attr('type') === 'password'){
        element.attr('type', 'text');
    }else {
        element.attr('type', 'password');
    }
});
/*==================================================
	makes sure that whole site is loaded
==================================================*/
$(window).on('load', function () {
    setTimeout(function () {
        $('#preloader-container').fadeOut('slow');
    },1000);
});
/* animation for home section after loaded page */
$(window).on('load', function () {
    $('.welcome-text').show().addClass("animated fadeInRightBig");
});
/*==================================================
	Show or Hide transparent black navigation
==================================================*/
// Close mobile menu on click
/*$(function(){

    $(".navbar-collapse ul li a").on("click touch", function(){

        $(".navbar-toggler").click();
    });
});*/
/*==================================================
	    side menu active
==================================================*/
$(function () {
    //active link side bar
    $('.side-menu.dash-menu ul li:not(:last-child) a').filter(function(){
        return this.href === location.href
    }).parent().addClass('active-side-menu-user').siblings().removeClass('active-side-menu-user');
    $('.side-menu.dash-menu ul li:not(:last-child) a').click(function(){
        $(this).parent().addClass('active-side-menu-user').siblings().removeClass('active-side-menu-user');
    });
});
/*==================================================
	    Smooth Scrolling
==================================================*/
$("a.smooth-scroll").click(function (event) {

    event.preventDefault();

    // get section id like #about, #servcies, #work, #team and etc.
    var section_id = $(this).attr("href");

    $("html, body").animate({
        scrollTop: $(section_id).offset().top - 90
    }, 1250, "easeInOutExpo");

});

var nav = document.querySelector("#navbar");
    var NavTop = nav.offsetTop;

    function fixnavbar(){
      if(window.scrollY >= NavTop){
        document.body.style.paddingTop = nav.offsetHeight + "px";
        document.body.classList.add("fixed-nav");
      }else {
        document.body.style.paddingTop = 0;
        document.body.classList.remove("fixed-nav");
      }
    }
    window.addEventListener("scroll", fixnavbar);
