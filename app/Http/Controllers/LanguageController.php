<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use Illuminate\Contracts\Session\Session as SessionSession;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;


class LanguageController extends Controller
{


        public function set_locale($lang)
        {
            app()->setLocale($lang);
            Session::put('applocale',$lang);
            return app()->getLocale();
        }

        public function get_conditions(){
            if(Session::get('applocale')=='en'){
                $data['title'] = '<b class="text-right border-bottom pb-2 border-secondary">Terms and Conditions</b>';
                $data['html'] = '<h5>read the arabic conditions</h5>';

            }else{
                $data['title'] = '<b class="text-right border-bottom pb-2 border-secondary">'.__('lang.terms_popup_header').'</b>';
                $data['html'] = __('lang.terms_popup_body');

            }

            echo json_encode($data);

    }

    public function saveApiData()
    {

        $res = Http::withHeaders([
            'X-API-KEY' => 'MobileAPP',
            'web-token' => 'bw&dB]F]?H_BS]g_8;2z!f`^4yU^tpfx',
            'platform'  => 'web'
        ])->get('https://mim.gov.sa/api/v1/cr/1010421448/details');

        echo $res->getStatusCode();
        // 200

        echo $res->getBody();
        // {"type":"User"...'

}


}
