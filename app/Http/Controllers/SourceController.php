<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use Illuminate\Contracts\Session\Session as SessionSession;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;


class SourceController extends Controller
{


    public function get_user_details()
    {

        $res = Http::withHeaders([
            'X-API-KEY' => 'MobileAPP',
            'web-token' => 'bw&dB]F]?H_BS]g_8;2z!f`^4yU^tpfx',
            'platform'  => 'web'
        ])->get('https://mim.gov.sa/api/v1/cr/1010421448/details');

        $string = $res->getBody()->getContents();
        $string = str_replace('\n', '', $string);
        $string = rtrim($string, ',');
        $string = "[" . trim($string) . "]";
        $json = json_decode($string, true);
        dd( $json[0]['result']['data']);
    }

    public function get_activities()
    {
        $res = Http::withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'locale'  => 'ar',
            'api_key'  => '21c77ad9d72f45a7b38443a103fd15e75de31827',
            'api_secret'  => 'a1bf575cd0b9445c1e331dd62b1b819aa85d865c',
        ])->post('https://inquiries.mim.gov.sa/index.php/api/v1/isic/getActivities',[
            "chapter_no" => 3,
        ]);
            $string = $res->getBody()->getContents();

            $string = str_replace('\n', '', $string);
            $string = rtrim($string, ',');
            $string = "[" . trim($string) . "]";
            $json = json_decode($string, true);
            return $json[0]['response']['ActivitiesList'];
    }


}
