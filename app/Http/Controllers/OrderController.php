<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\User;
use App\Models\Notification;
use Illuminate\Http\Request;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
use App\Http\Resources\OrderResource;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request , $id)
    {
        $array_data = serialize($request->variables);
        // $old_arr = unserialize($array_data);
        // dd($old_arr);
        // dd($request->all());

        $order = new Order();
        $order->title = $request->title;
        $order->trader_id = $request->user_id;
        $order->sector_id = $id;
        $order->status = 'pending';
        $order->details =$array_data ;
        $order->comments = $request->details;
        $order->save();

        $notification = new Notification();
        $req = [];
        $req['destination_id']=$id;
        $req['title']='New Order';
        $req['body']='New Order Created with id' .$order->id ;
        $req['type']='sector';
        $req['url']=$order->id;

        app('App\Http\Controllers\NotificationController')->add_notification($req);


        return new OrderResource($order);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
    public function my_orders($id)
    {
        $orders = Order::where('trader_id',$id)->with('sector')->get();
        return new DataTableCollectionResource($orders);
    }
    public function sector_orders($id)
    {
        return OrderResource::collection(Order::where('sector_id',$id)->with('trader')->get());
    }
    public function get_order($id)
    {
        $order = Order::where('id',intval($id))->with('trader')->get();
        $order[0]['details'] = unserialize($order[0]['details']);
        return $order->toArray();
    }
    public function adminViewOrders()
    {
        $orders = Order::with(['sector','trader'])->get();
        return new DataTableCollectionResource($orders);
    }
}
