<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use Illuminate\Contracts\Session\Session as SessionSession;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
use Illuminate\Support\Facades\Session;


class AdminController extends Controller
{
    public function __construct()
        {
            $this->middleware('auth');
        }

        public function index()
        {
            if (request()->user()->hasRole('admin')) {
                return view('admin.dashboard');
            }

            // if (request()->user()->hasRole('user')) {
            //     return redirect('/home');
            // }
        }

        public function set_locale($lang)
        {
            app()->setLocale($lang);
            Session::put('locale',$lang);
            return app()->getLocale();
        }




}
