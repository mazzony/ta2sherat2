<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Resources\UserResource;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
use Symfony\Component\HttpFoundation\Session\Session;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        return UserResource::collection(User::latest()->paginate(5));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'firstName' => 'required',
            'middleName' => 'required',
            'lastName' => 'required',
            'email' => 'required',
        ]);

        $post = new User;
        $post->firstName = $request->firstName;
        $post->middleName = $request->middleName;
        $post->lastName = $request->lastName;
        $post->email = $request->email;
        $post->password = bcrypt($request->password);
        $post->status = $request->status;
        $post->type = 'user';
        $post->save();
        $post->roles()->attach(\App\Models\Role::where('name', 'user')->first());


        return new UserResource($post);
    }
    public function show(User $user)
    {
        return new UserResource($user);
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return response()->json(null, 204);
    }

    public function getUnActivated (Request $request)
    {
        $length = $request->input('length');
        $sortBy = $request->input('column');
        $orderBy = $request->input('dir');
        $searchValue = $request->input('search');
        //$query = User::eloquentQuery($sortBy, $orderBy, $searchValue);
        $query = User::where('type','<>','system_user')->where('type','<>','');

        if($searchValue){
            $query = $query->where('name','Like','%'.$searchValue.'%')->orWhere('email','Like','%'.$searchValue.'%');
        }

        //$query= $query->C($length);

        $data = $query->orderBy($sortBy,$orderBy)->paginate($length);

        return new DataTableCollectionResource($data);




        //return response()->json($data);
    }

    public function change_status($id)
    {
        $user=User::find($id);
        if($user->status== "active"){
            $user->status ="inactive";
        }else{
            $user->status ="active";
        }

        $user->save();
    }
    public function list_admins(Request $request)
    {
        //$id= session()->get('user')->id;
        $length = $request->input('length');
        $sortBy = $request->input('column');
        $orderBy = $request->input('dir');
        $searchValue = $request->input('search');
        $searchString="";
        //$query = User::eloquentQuery($sortBy, $orderBy, $searchValue);
        //$query = User::where('type','=',"system_user")->where('id','<>',$id);
        $query = User::where('type','=',"system_user");
        if($searchValue){
            $query = $query->where('name','Like','%'.$searchValue.'%')->orWhere('email','Like','%'.$searchValue.'%');
        }

        $query = $query->whereHas('roles', function ($querys) use ($searchString){
            $querys->where('name', 'admin');
        });
        //$query= $query->C($length);

        $data = $query->orderBy($sortBy,$orderBy)->paginate($length);



        return new DataTableCollectionResource($data);




        //return response()->json($data);
    }

    public function list_evaluators(Request $request)
    {
        //$id= session()->get('user')->id;
        $length = $request->input('length');
        $sortBy = $request->input('column');
        $orderBy = $request->input('dir');
        $searchValue = $request->input('search');
        $searchString="";
        //$query = User::eloquentQuery($sortBy, $orderBy, $searchValue);
        //$query = User::where('type','=',"system_user")->where('id','<>',$id);
        $query = User::where('type','=',"evaluator");
        if($searchValue){
            $query = $query->where('name','Like','%'.$searchValue.'%')->orWhere('email','Like','%'.$searchValue.'%');
        }

        $query = $query->whereHas('roles', function ($querys) use ($searchString){
            $querys->where('name', 'evaluator');
        });
        //$query= $query->C($length);

        $data = $query->orderBy($sortBy,$orderBy)->paginate($length);



        return new DataTableCollectionResource($data);




        //return response()->json($data);
    }

    public function list_super_evaluators(Request $request)
    {
        //$id= session()->get('user')->id;
        $length = $request->input('length');
        $sortBy = $request->input('column');
        $orderBy = $request->input('dir');
        $searchValue = $request->input('search');
        $searchString="";
        //$query = User::eloquentQuery($sortBy, $orderBy, $searchValue);
        //$query = User::where('type','=',"system_user")->where('id','<>',$id);
        $query = User::where('type','=',"super_evaluator");
        if($searchValue){
            $query = $query->where('name','Like','%'.$searchValue.'%')->orWhere('email','Like','%'.$searchValue.'%');
        }

        // $query = $query->whereHas('roles', function ($querys) use ($searchString){
        //     $querys->where('name', 'super_evaluator');
        // });
        //$query= $query->C($length);

        $data = $query->orderBy($sortBy,$orderBy)->paginate($length);



        return new DataTableCollectionResource($data);




        //return response()->json($data);
    }

    public function add_admin (Request $request)
    {

        $user = new User;
        $user->firstName = $request->firstName;
        $user->middleName = $request->middleName;
        $user->lastName = $request->lastName;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);;
        $user->type = "system_user";
        $user->status = "active";
        $user->save();
        $user->roles()->attach(\App\Models\Role::where('name', 'admin')->first());
        return new UserResource($user);

    }
    public function add_evaluator (Request $request)
    {

        $user = new User;
        $user->firstName = $request->firstName;
        $user->middleName = $request->middleName;
        $user->lastName = $request->lastName;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);;
        $user->type = "evaluator";
        $user->status = "active";
        $user->save();
        $user->roles()->attach(\App\Models\Role::where('name', 'evaluator')->first());
        return new UserResource($user);

    }
    public function add_superevaluator (Request $request)
    {

        $user = new User;
        $user->firstName = $request->firstName;
        $user->middleName = $request->middleName;
        $user->lastName = $request->lastName;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);;
        $user->type = "super_evaluator";
        $user->status = "active";
        $user->save();
        $user->roles()->attach(\App\Models\Role::where('name', 'evaluator')->first());
        return new UserResource($user);

    }
    public function get_admin($id)
    {
        $user = User::find($id);
        return new UserResource($user);
    }

    public function update_admin(Request $request ,$id)
    {
        $user = User::find($id);
        $user->update($request->only(['firstName','middleName','lastName', 'email']));
        return new UserResource($user);
    }

    public function get_current_user(){
        $user = auth()->user();
        return $user;
    }

    public function get_type(){
        $user = auth()->user();
        return $user['type'];
    }
    public function get_user_id(){
        $user = auth()->user();
        return $user['id'];
    }
    public function get_user_sector(){
        $user = auth()->user();
        return $user['sector'];
    }

    public function user_dashboad(){
        if (request()->user()->hasRole('admin')) {
            return view('admin.dashboard');
        }else{
            return view('user.dashboard');
        }
    }

    public function get_profile(){
        $user = auth()->user();
        return $user;
    }
    //////Factories Function ///////

    public function list_factories(Request $request)
    {

        $length = $request->input('length');
        $sortBy = $request->input('column');
        $orderBy = $request->input('dir');
        $searchValue = $request->input('search');
        $searchString="";
        $query = User::where('type','=',"factory");

        if($searchValue){
            $query = $query->where('name','Like','%'.$searchValue.'%')->orWhere('email','Like','%'.$searchValue.'%');
        }
        $data = $query->orderBy($sortBy,$orderBy)->paginate($length);
        return new DataTableCollectionResource($data);

    }


    /////Quarries functions/////////
    public function list_quarries(Request $request)
    {
        $length = $request->input('length');
        $sortBy = $request->input('column');
        $orderBy = $request->input('dir');
        $searchValue = $request->input('search');
        $searchString="";
        $query = User::where('type','=',"quarry");

        if($searchValue){
            $query = $query->where('name','Like','%'.$searchValue.'%')->orWhere('email','Like','%'.$searchValue.'%');
        }
        $data = $query->orderBy($sortBy,$orderBy)->paginate($length);
        return new DataTableCollectionResource($data);
    }


    //////////Traders Functions //////////////
    public function list_traders(Request $request)
    {
        $length = $request->input('length');
        $sortBy = $request->input('column');
        $orderBy = $request->input('dir');
        $searchValue = $request->input('search');
        $searchString="";
        $query = User::where('type','=',"trader");

        if($searchValue){
            $query = $query->where('name','Like','%'.$searchValue.'%')->orWhere('email','Like','%'.$searchValue.'%');
        }
        $data = $query->orderBy($sortBy,$orderBy)->paginate($length);
        return new DataTableCollectionResource($data);
    }






}
