<?php

namespace App\Http\Controllers;

use App\Models\Sector;
use Illuminate\Http\Request;
use App\Http\Resources\SectorResource;
use Illuminate\Support\Facades\Http;

class SectorController extends Controller
{
    private $cr;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SectorResource::collection(Sector::latest()->paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'file' => 'required|mimes:jpg,jpeg,png|max:2048',
        ]);
        if($request->file()) {
            $file_name = time().'_'.$request->file->getClientOriginalName();
            $file_path = $request->file('file')->storeAs('uploads', $file_name, 'public');
        }else{
            $file_name = 'default-image.png';
        }

        $post = new Sector;
        $post->name = $request->name;
        $post->description = $request->description;
        $post->parent = $request->parent;
        $post->region = $request->region;
        $post->featured_image = $file_name;
        $post->save();

        return new SectorResource($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function show(Sector $sector)
    {
        return new SectorResource($sector);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function edit(Sector $sector)
    {
      //
    }

    public function view($id)
    {
        dd($id);
        $sector = Sector::find($id);
        return new SectorResource($sector);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sector $sector)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $sector->update($request->only(['name', 'description']));

        return new SectorResource($sector);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sector $sector)
    {
        $sector->delete();

        return response()->json(null, 204);
    }

    public function all()
    {
        return view('landing', [
            'sectors' => Sector::latest()->paginate(5)
        ]);
    }

    public function single($id)
    {
        $sector = Sector::find($id);
        return view('single', compact('sector'));
    }

    public function getSectors()
    {
        $data = Sector::get();

        return response()->json($data);
    }

    public function getSectorsFe(Request $request)
    {
        $search = $request->search;

      if($search == ''){
        $sectors = Sector::orderby('name','asc')->select('id','name')->limit(5)->get();
      }else{
        $sectors = Sector::orderby('name','asc')->select('id','name')->where('name', 'like', '%' .$search . '%')->limit(5)->get();
      }

        $data = array();
      foreach($sectors as $employee){
         $data[] = array(
              "id"=>$employee->id,
              "text"=>$employee->name
         );
      }

        return response()->json($data);
    }
    public function get_user_sectors($id)
    {
        //return SectorResource::collection(Sector::where('id',$id)->paginate(10));
        $res = Http::withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'locale'  => 'ar',
            'api_key'  => '21c77ad9d72f45a7b38443a103fd15e75de31827',
            'api_secret'  => 'a1bf575cd0b9445c1e331dd62b1b819aa85d865c',
        ])->post('https://inquiries.mim.gov.sa/index.php/api/v1/isic/getActivities',[
            "chapter_no" => 3,
        ]);
            $string = $res->getBody()->getContents();
            $string = str_replace('\n', '', $string);
            $string = rtrim($string, ',');
            $string = "[" . trim($string) . "]";
            $json = json_decode($string, true);
            $this->cr = $id;
            $results = array_filter($json[0]['response']['ActivitiesList'], function($people) {
                return $people['ActivityKey'] == intval($this->cr);
              });

              return($results);
           // return $json[0]['response']['ActivitiesList'];
    }

}
