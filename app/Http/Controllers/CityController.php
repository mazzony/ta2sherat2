<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        //
    }
    public function add_cities(){

        $arabic =array(
            "الرياض",
            "جدة",
            "مكة المكرمة",
            "المدينة المنورة",
            "الاحساء",
            "الدمام",
            "الطائف",
            "بريدة",
            "تبوك",
            "القطيف",
            "خميس مشيط",
            "الخبر",
            "حفر الباطن",
            "الجبيل",
            "الخرج",
            "أبها",
            "حائل",
            "نجران",
            "ينبع",
            "صبيا",
            "الدوادمي",
            "بيشة",
            "أبو عريش",
            "القنفذة",
            "محايل",
            "سكاكا",
            "عرعر",
            "عنيزة",
            "القريات",
            "صامطة",
            "جازان",
            "المجمعة",
            "القويعية",
            "احد المسارحه",
            "الرس",
            "وادي الدواسر",
            "بحرة",
            "الباحة",
            "الجموم",
            "رابغ",
            "أحد رفيدة",
            "شرورة",
            "الليث",
            "رفحاء",
            "عفيف",
            "العرضيات",
            "العارضة",
            "الخفجي",
            "بالقرن",
            "الدرعية",
            "ضمد",
            "طبرجل",
            "بيش",
            "الزلفي",
            "الدرب",
            "الافلاج",
            "سراة عبيدة",
            "رجال المع",
            "بلجرشي",
            "الحائط",
            "ميسان",
            "بدر",
            "املج",
            "رأس تنوره",
            "المهد",
            "الدائر",
            "البكيريه",
            "البدائع",
            "خليص",
            "الحناكية",
            "العلا",
            "الطوال",
            "النماص",
            "المجاردة",
            "بقيق",
            "تثليث",
            "المخواة",
            "النعيرية",
            "الوجه",
            "ضباء",
            "بارق",
            "طريف",
            "خيبر",
            "أضم",
            "النبهانية",
            "رنيه",
            "دومة الجندل",
            "المذنب",
            "تربه",
            "ظهران الجنوب",
            "حوطة بني تميم",
            "الخرمة",
            "قلوه",
            "شقراء",
            "المويه",
            "المزاحمية",
            "الأسياح",
            "بقعاء",
            "السليل",
            "تيماء",
        );

        $english =array("Riyadh",
        "Jeddah",
        "Makkah",
        "Medina",
        "Hasa",
        "Dammam",
        "Taif",
        "Buraydah",
        "Tabuk",
        "Qatif",
        "Khamis Mushait",
        "the news",
        "Hafar Al-Batin",
        "Jubail",
        "Kharj",
        "Abha",
        "Hail",
        "Najran",
        "Yanbu",
        "Sebya",
        "Dawadmi",
        "Bishah",
        "Abu Arish",
        "Al Qunfudhah",
        "Mahael",
        "skaka",
        "Arar",
        "Unaizah",
        "Qurayyat",
        "Samtah",
        "Jazan",
        "collected",
        "Al-Quway'iyah",
        "One Theatre",
        "Alrass",
        "Wadi Al Dawasir",
        "Bahra",
        "patio",
        "Algamom",
        "Rabigh",
        "Ahad Rafaida",

"shrura",
"allith",
"rafaha'",
"efifa",
"aleardiaati",
"alearidati",
"alkhfiji",
"balqarna",
"aldareiatu",
"damad",
"tbirjil",
"bisha",
"alzilfi",
"aldirba",
"alaflaj",
"sarat eubaydata",
"rjal almae",
"biljarshi",
"alhayit",
"misan",
"bdra",
"amilja",
"ras tanurh",
"almuhada",
"aldaayiri",
"albkirih",
"albadayiei",
"khlis",
"alhnakiati",
"aleala",
"altawali",
"alnamasi",
"almujaradati",
"baqiqu",
"tathlithi",
"almakhuati",
"alnueiriati",
"alwajahi",
"dba'i",
"bariq",
"trif",
"khaybar",
"'adma",
"alnabhania",
"rnihi",
"dumat aljandalu",
"almudhanbi",
"trbih",
"zahran aljanuba",
"hutat bani tamim",
"alkharimati",
"qluhu",
"shqra'i",
"almwyhi",
"almizahimiati",
"al'asyahi",
"baqea'a",
"alsalil",
"tima'"
    );

    foreach($arabic as $key => $ar){
        $city = new City();
        $city->name_ar = $ar;
        $city->name_en = $english[$key];
        $city->save();

    }
    dd('done');
    }

    public function get_cities(){
        $cities = City::all();
        return $cities;
    }
}
