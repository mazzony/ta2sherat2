<?php

namespace App\Http\Controllers;

use App\Models\Requests;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\RequestResource;
use App\Models\Order;
use App\Models\City;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\StatisticsExport;

// require_once(dirname(__FILE__,3).'\Providers\TCPDF\tcpdf.php');
require (__DIR__.'/../../Providers/TCPDF/tcpdf.php');

use TCPDF;
use Illuminate\Support\Facades\Mail;
use Haruncpi\LaravelIdGenerator\IdGenerator;

class RequestController extends Controller
{
    protected $email , $name;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Requests $requests)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
    }

    public function add(Request $request)
    {
       // dd(auth()->user()->id);
//dd($request->all());
//dd($request->comments);
        $array = [];
        // $count = count($request->variables);
        // for( $i=0;$i<$count;$i++){
        //     $object = [];
        //     $object['employeeName']= $request->quantity[$i]['employeeName'];
        //     $object['passNumber']= $request->period[$i];
        //     $object['visaNumber']= $request->price[$i];

        //     array_push($array,$object);
        // }

            $upload_location='formuploads/';
            //$files_arr =array();
            if(isset($_FILES['files']['name'])){
                $files = $request->file('files');
                $i=0;
                foreach($files as $file){
                $i++;
                    $fileName = strval($i).time().'.'.$file->getClientOriginalExtension();
                    $file->move(public_path('formuploads'), $fileName);
                    $files_arr[] = $fileName;

                }

                }


                $id =auth()->user()->id;

        $user= User::find(auth()->user()->id);
        $requests = new Requests();
        $requests->firstName = $request->firstName;
        $requests->middleName = $request->middleName;
        $requests->lastName = $request->lastName;
        $requests->cr = $request->CR;
        $requests->ir = $request->IR;
        $requests->factory_name = $request->FactoryName;
        $requests->factory_symbol = $request->FactorySymbol;
        $requests->phone = $request->phone;
        $requests->email = $request->email;
        $requests->from = $id;
        $requests->phone = $request->phone;
        $requests->city = intval($request->city);
        $requests->details = serialize(json_decode($request->variables));
        $requests->status = 'new';
        if(isset($_FILES['files']['name'])){
            $requests->attachments = serialize($files_arr);
        }
        $requests->save();
        $requests->s_id = strval($requests->id);
        $requests->request_id  = IdGenerator::generate(['table' => 'requests','field'=>'request_id', 'length' => 12, 'prefix' => 'REQ-','reset_on_prefix_change'=>true]);
        $requests->save();

        $this->email = $requests->email;
        $this->name = $requests->firstName . ' ' . $requests->lastName;

        $data = array('name'=>$this->name,'id'=>$requests->id,'status'=>$requests->status);
        Mail::send('mail', $data, function($message) {
            $message->to($this->email, $this->name)->subject
               ('خدمة استثناء خروج وعودة');
            $message->from('ihd@mim.gov.sa','خدمة استثناء خروج وعودة');
         });


        return new RequestResource($request);
    }

    public function update_request($id, Request $request)
    {
       // dd($request->all());
        $upload_location='formuploads/';
        //$files_arr =array();
        if(isset($_FILES['files']['name'])){
            $files = $request->file('files');
            $i=0;
            foreach($files as $file){
            $i++;
                $fileName = strval($i).time().'.'.$file->getClientOriginalExtension();
                $file->move(public_path('formuploads'), $fileName);
                $files_arr[] = $fileName;

            }

            }

            $user_id =auth()->user()->id;


        $user= User::find(auth()->user()->id);
        $requests = Requests::find($id);
        $requests->firstName = $request->firstName;
        $requests->middleName = $request->middleName;
        $requests->lastName = $request->lastName;
        $requests->cr = $request->CR;
        $requests->ir = $request->IR;
        $requests->factory_name = $request->FactoryName;
        $requests->factory_symbol = $request->FactorySymbol;
        $requests->phone = $request->phone;
        $requests->email = $request->email;
        $requests->from = $user_id;
        $requests->phone = $request->phone;
        $requests->city = intval($request->city);
        $requests->details = serialize(json_decode($request->variables));
        if(isset($_FILES['files']['name'])){
            $requests->attachments = serialize($files_arr);
        }
        $requests->status = 'edited_by_user';
        $requests->save();


        $this->email = $requests->email;
        $this->name = $requests->firstName . ' ' . $requests->lastName;

        $data = array('name'=>$this->name,'id'=>$requests->id,'status'=>$requests->status);
        Mail::send('mail', $data, function($message) {
            $message->to($this->email, $this->name)->subject
               ('خدمة استثناء خروج وعودة');
            $message->from('ihd@mim.gov.sa','خدمة استثناء خروج وعودة');
         });
        return new RequestResource($request);
    }

    public function my_requests($id)
    {
        $orders = Requests::where('from',$id)->with(['to','order'])->get();
        return new DataTableCollectionResource($orders);
    }



    public function order_requests($id)
    {
        $requests = Requests::where('order_id',$id)->with(['from','order'])->get();
        return new DataTableCollectionResource($requests);
    }

    public function admin_requests($id)
    {
        $requests = Requests::where('order_id',$id)->where('status','Accepted')->with(['from','to','order'])->get();
        return new DataTableCollectionResource($requests);
    }

    public function get_request($id)
    {
        $requests = Requests::find($id);
         //dd($requests->details);

        $requests['details']= unserialize($requests->details);
        $requests['attachments']= unserialize($requests->attachments);
        $response['request'] = $requests;
        $response['city'] = City::find($requests->city);
        return $response;

    }

    public function trader_get_request($id)
    {
        $requests = Requests::find($id);
        $requests['details']= unserialize($requests->details);
        $order = Order::find($requests->order_id);
        $order['details']= unserialize($order->details);
        $to = User::find($requests->from);
        $response = [];
        $response['request'] = $requests;
        $response['order'] = $order;
        $response['to'] = $to;
        return $response;

    }

    public function admin_get_request($id)
    {
        $requests = Requests::find($id);
        $requests['details']= unserialize($requests->details);
        $order = Order::find($requests->order_id);
        $order['details']= unserialize($order->details);
        $to = User::find($requests->from);
        $from = User::find($requests->to);
        $response = [];
        $response['request'] = $requests;
        $response['order'] = $order;
        $response['to'] = $to;
        $response['from'] = $from;
        return $response;

    }

    public function accept($id)
    {
        $requests = Requests::find($id);
        $requests->status='initial_acceptance';
        $requests->save();

        $req = [];
        $req['destination_id']=$requests->from;
        $req['title']='Request Accepted';
        $req['body']='Yor Request' .$requests->id.'Accepted' ;
        $req['type']='request_status';
        $req['url']=$requests->id;
        $req['post_id']=$requests->id;


        app('App\Http\Controllers\NotificationController')->add_notification($req);



    }

    public function return_to_eval($id , Request $request)
    {
        $requests = Requests::find($id);
        if($request->comment){
            $requests->certifier_comment= $request->comment;
        }
        $requests->status='return_to_evaluator';
        $requests->certifier = auth()->user()->id;
        $requests->save();





    }
    public function refuse($id)
    {
        $requests = Requests::find($id);
        $requests->status='Refused';
        $requests->save();

        $req = [];
        $req['destination_id']=$requests->from;
        $req['title']='Request refused';
        $req['body']='Yor Request' .$requests->id.'refused' ;
        $req['type']='request_status';
        $req['url']=$requests->id;
        $req['post_id']=$requests->id;


        app('App\Http\Controllers\NotificationController')->add_notification($req);

    }

    public function admin_accept($id)
    {
        $requests = Requests::find($id);
        $requests->admin_approval='Accepted';
        $requests->save();

        // $req = [];
        // $req['destination_id']=$requests->from;
        // $req['title']='Request Accepted';
        // $req['body']='Yor Request' .$requests->id.'Accepted' ;
        // $req['type']='request_status';
        // $req['url']=$requests->id;
        // $req['post_id']=$requests->id;


        // app('App\Http\Controllers\NotificationController')->add_notification($req);



    }
    public function admin_refuse($id)
    {
        $requests = Requests::find($id);
        $requests->admin_approval='Refused';
        $requests->save();

        // $req = [];
        // $req['destination_id']=$requests->from;
        // $req['title']='Request refused';
        // $req['body']='Yor Request' .$requests->id.'refused' ;
        // $req['type']='request_status';
        // $req['url']=$requests->id;
        // $req['post_id']=$requests->id;


      //  app('App\Http\Controllers\NotificationController')->add_notification($req);

    }

    public function adminViewRequests($type)
    {
        $requests = Requests::whereHas('from', function ($query) use ($type){
            $query->where('type',$type);
        })->with(['sector','to','order','from'])->get();
        return new DataTableCollectionResource($requests);
    }

    public function delete_request($id)
    {
        $Requests = Requests::find($id);
        $Requests->delete();

        return response()->json(null, 204);
    }

    public function listRequests (Request $request)
    {
        //$id= session()->get('user')->id;
        $length = $request->input('length');
        $sortBy = $request->input('column');
        $orderBy = $request->input('dir');
        $searchValue = $request->input('search');
        $searchString="";
        //$query = User::eloquentQuery($sortBy, $orderBy, $searchValue);
        //$query = User::where('type','=',"system_user")->where('id','<>',$id);
        $query = Requests::where('factory_name','<>',null);
        if($searchValue){
            $query = $query->whereHas('req_city', function ($ss) use ($searchValue){

                $ss->where('name_ar','Like','%'.$searchValue.'%');

            });

            $query = $query->orwhere('factory_name','Like','%'.$searchValue.'%')->orWhere('factory_symbol','Like','%'.$searchValue.'%')
            ->orWhere('cr','Like','%'.$searchValue.'%');
        }

        $isActive = $request->input('isActive');
        $request_number = $request->input('request_number');

        if (isset($isActive)) {
            $query =  $query->where("status", $isActive);
        }
        // if($request_number){
        //     $query = $query->where('request_id','Like','%'.$request_number.'%');
        // }
        if($request_number){
            $query = $query->where('id',intval($request_number));
        }
        //$query= $query->C($length);

        $data = $query->with(['req_city'])->orderBy('created_at','desc')->paginate($length);



        return new DataTableCollectionResource($data);




        //return response()->json($data);
    }
    public function userListRequests(Request $request){
        $id= session()->get('user')->id;
        $length = $request->input('length');
        $sortBy = $request->input('column');
        $orderBy = $request->input('dir');
        $searchValue = $request->input('search');
        $searchString="";
        //$query = User::eloquentQuery($sortBy, $orderBy, $searchValue);
        //$query = User::where('type','=',"system_user")->where('id','<>',$id);
        $query = Requests::where('from',$id);
        if($searchValue){
            $query = $query->whereHas('req_city', function ($ss) use ($searchValue){

                $ss->where('name_ar','Like','%'.$searchValue.'%');

            });

            $query = $query->orwhere('factory_name','Like','%'.$searchValue.'%')->orWhere('factory_symbol','Like','%'.$searchValue.'%')
            ->orWhere('cr','Like','%'.$searchValue.'%');
        }

        $isActive = $request->input('isActive');
        $request_number = $request->input('request_number');

        if (isset($isActive)) {
            $query =  $query->where("status", $isActive);
        }
        if($request_number){
            $query = $query->where('id','Like',intval($request_number));
           // $query = $query->where('request_id','Like','%'.$request_number.'%');
        }



        //$query= $query->C($length);

        $data = $query->with(['req_city'])->orderBy('created_at','desc')->paginate($length);



        return new DataTableCollectionResource($data);


    }
    public function set_processing($id){
        $requests = Requests::find($id);
        $requests->status = 'processing';
        $requests->save();

    }

     public function sendMail()
    {
        $data = array('name'=>"Mazen Yehia");
        Mail::send('mail', $data, function($message) {
            $message->to('mazzony13@outlook.sa', 'Mazen')->subject
               ('Ta2sherat');
            $message->from('ihd@mim.gov.sa','Mazen Yehia');
         });
        //  dd(123);
    }

    public function set_processed($id, Request $request){

        $requests = Requests::find($id);
        $requests->status='initial_acceptance';
        $requests->response_comments= $request->comment;
        $requests->responser = auth()->user()->id;
        $requests->save();


        $this->email = $requests->email;
        $this->name = $requests->firstName . ' ' . $requests->lastName;
        $requests['details']= unserialize($requests->details);
        $from = User::find($requests->from);
        $data = array('name'=>$this->name,'id'=>$requests->id,'status'=>$requests->status);
        Mail::send('mail', $data, function($message) {
            $message->to( $this->email , $this->name)->subject
               ('خدمة استثناء خروج وعودة');
            $message->from('ihd@mim.gov.sa','خدمة استثناء خروج وعودة');
         });


         $responder = null;
         if($requests->responser){
             $responder=User::find($requests->responser);

         }
         $employee_text='';
         $employee_text_2='';
         $employee_text_3='';
          if(count($requests['details'])==1){
             $employee_text = 'الموظف الحاصل';
             $employee_text_2 = 'بياناته';
             $employee_text_3 = 'دخوله';
          }else if(count($requests['details'])>1){
             $employee_text = 'الموظفين الحاصلين';
             $employee_text_2 = 'بياناتهم';
             $employee_text_3 = 'دخولهم';
          }
         $html='<style>
         td {border: 0px dotted #fff; }
         th {border: 1px dotted #000; }
         </style>';

         // $html.='<table cellpadding="10">';
         // $html.='<tr style="direction:ltr !important;"> <td  style="border: 0px dotted #000;"> </td> <td><img src="/tamken/public/assets/img/header.jpg" style="width:200;" /> </td> <td> </td></tr>';
         // $html.='</table>';

         // $html.='<img class="img" src="/tamken/public/assets/img/header.jpg"/>';


         $html.='<p class="am-label" style="text-align:left; width:50px;font-size:13px;" >الموضوع: بشأن طلب تسهيل إجراءات دخول
         موظف</p>';
         $html.='<p class="am-label" style="text-align:left">'.$requests->factory_name.'</p>';
         $html.='<p class="am-label" style="text-align:center">برقية سرية وعاجلة جدًا</p>';

         $html.='<table cellpadding="10">';
         $html.='<tr><td colspan="2">معالي مدير عام الجوازات	</td><td></td> <td></td><td>       حفظه الــله</td></tr>';
         $html.='<tr><td colspan="2">السلام عليكم ورحمة الــله وبركاته</td><td></td> <td></td></tr>';
         $html.='</table>';


         $html.='<br>';

         $html.='<p class="am-label" >
         إشارة إلى برقية صاحب السمو الملكي وزير الداخلية رقم (150679) وتاريخ 12/7/1442ه، المشار فيها إلى برقية معالي رئيس اللجنة التنفيذية الأمنية رقم (296) وتاريخ 10/7/1442ه، المتضمنة رأي اللجنة التنفيذية الأمنية مناسبة ما رآه معالي وزير الصحة في البرقية رقم (1223682) وتاريخ 1/7/1442ه، بشأن الموافقة على دخول المهندسين والفنيين، من رعايا بعض الدول المشمولة في قرار المنع من دخول المملكة بسبب جائحة (كورونا)، وإشارة إلى التعهد والالتزام المقدم من '.$requests->factory_name.' لإجراءات الاحترازية للحد من تفشي فايروس (كورونا) (مرفق بكامل مرفقاته)، الخاص بطلب عودة '.$employee_text.' على تأشيرة خروج وعودة، الموضحة '.$employee_text_2.' في الجدول أدناه.

         </p>';
         $html.='<br>';
         $html.='<br>';


         $html.='<br>';
         $html.='<br>';
         $html.='<br>';

         $html.='<table cellpadding="10" style="border: 1px dotted #fff; !important ">';
         $html.='<tr> <th> مسلسل</th> <th> اسم الموظف</th> <th> رقم الجواز</th> <th> رقم التأشيرة</th> <th colspan="2"> خط سير العمل</th> </tr>';
         $html.=' <tr>
         <th style="font-size:14px !important; border:none !important; border-right:1px solid #000;" ></th>
         <th style="font-size:14px !important; border:none !important;" ></th>
         <th style="font-size:14px !important; border:none !important;" ></th>
         <th style="font-size:14px !important; border:none !important;" ></th>
         <th style="font-size:14px !important; " >من</th>
         <th style="font-size:14px !important; " >الى</th>
          <th style="font-size:14px !important; border:none !important;" ></th>

     </tr>';


         for($i=0; $i<count($requests['details']);$i++){



             $html.='<tr><td  style="border: 0px dotted #000;">'. strval($i+1) .'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->employeeName.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->passNumber.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->visaNumber.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->from.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->to.'</td> </tr>';

         }


         $html.='</table>';


         $html.='<br>';
         $html.='<br>';

         $html.='<p class="am-label" style="line-height:30px;" >
         آمل من معاليكم تسهيل إجراءات '.$employee_text_3.'، ويمكن التنسيق في ذلك مع سعادة الأستاذ/ ماجد بن صالح الصيخان، على الجوال رقم (0544887750)، أو البريد الإلكتروني (sa.gov.mim@saikhan.majed).
         </p>';

         $html.='<br>';
         $html.='<br>';

         $html.='<p class="am-label" style="text-align:center" >
         وتقبلوا معاليكم خالص تحياتي
         </p>';

         $html.='<p class="am-label" style="text-align:left" >
         نائب وزير الصناعة والثروة المعدنية
         </p>';
         $html.='<br>';
         $html.='<br>';
         $html.='<br>';
         $html.='<br>';
         $html.='<br>';
         $html.='<br>';
         $html.='<p class="am-label" style="text-align:left" >
         م. أسامة بن عبد العزيز الزامل
         </p>';




         // $html.='<br>';
         // $html.='<br>';
        // $html.='<table cellpadding="10">';

        // $html.='<tr><td> توقيع العميل</td> <td></td> <td></td> <td></td> <td> توقيع المستثمر</td></tr>';

        // $html.='<tr><td> - - - - - - - - - -  </td> <td></td> <td></td> <td></td> <td>- - - - - - - - - - -</td></tr>';

        // $html.='</table>';

// create new PDF document
        $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set default header data
        // $pdf->SetHeaderData('amen_logo.png', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
      //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
        $lg = Array();
        $lg['a_meta_charset'] = 'UTF-8';
        $lg['a_meta_dir'] = 'rtl';
        $lg['a_meta_language'] = 'fa';
        $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
        $pdf->setLanguageArray($lg);

// ---------------------------------------------------------
// set font
        $pdf->SetFont('dejavusans', '', 12);
// add a page
        $pdf->AddPage();
        $pdf->WriteHTML($html, true, 0, true, 0);
// set LTR direction for english translation
        $pdf->setRTL(false);
        $pdf->SetFontSize(10);
// print newline
        $pdf->Ln();
//Close and output PDF document
        $pdf->Output("Contract_ar".$id.".pdf",'D');
    //   $pdf->Output("C:\\xampp\\htdocs\\ta2shera\\public\\PDF\\Contract_ar".$id.".pdf",'F');
//============================================================+
// END OF FILE
//============================================================+
    }

    public function set_refused($id, Request $request){



        $requests = Requests::find($id);
        $requests->status='refused';
        $requests->response_comments= $request->comment;
        $requests->responser = auth()->user()->id;
        $requests->save();


        $this->email = $requests->email;
        $this->name = $requests->firstName . ' ' . $requests->lastName;
        $requests['details']= unserialize($requests->details);
        $from = User::find($requests->from);
        $data = array('name'=>$this->name,'id'=>$requests->id,'status'=>$requests->status);
        Mail::send('mail', $data, function($message) {
            $message->to( $this->email , $this->name)->subject
               ('خدمة استثناء خروج وعودة');
            $message->from('ihd@mim.gov.sa','خدمة استثناء خروج وعودة');
         });


//          $responder = null;
//          if($requests->responser){
//              $responder=User::find($requests->responser);

//          }

//         $html='<style>
//         td {border: 0px dotted #fff; }
//         th {border: 1px dotted #000; }
//         </style>';

//         // $html.='<table cellpadding="10">';
//         // $html.='<tr style="direction:ltr !important;"> <td  style="border: 0px dotted #000;"> </td> <td><img src="/tamken/public/assets/img/header.jpg" style="width:200;" /> </td> <td> </td></tr>';
//         // $html.='</table>';

//         // $html.='<img class="img" src="/tamken/public/assets/img/header.jpg"/>';


//         $html.='<p class="am-label" style="text-align:center">طلب رقم '.$id.'</p>';
//         $html.='<table cellpadding="10">';
//         $html.='<tr><td><p style=" text-decoration: underline;"> بيانات المتقدم </p></td><td></td> </tr>';

//         $html.='<tr><td>الاسم : '. $from->firstName .' '. $from->middleName . ' '. $from->lastName . '</td><td></td><td></td></tr>';
//         $html.='<tr><td>البريد الالكتروني :'. $from->email .'</td><td></td><td></td></tr>';
//         $html.='<tr><td>الهاتف : '. $from->phone .'</td><td></td><td></td></tr>';
//         $html.='</table>';


//         $html.='<br>';
//         $html.='<br>';
//         $html.='<br>';
//         $html.='<tr><td><p style=" text-decoration: underline;"> بيانات الطلب </p></td><td></td> </tr>';

//         $html.='<br>';
//         $html.='<br>';

//         $html.='<table cellpadding="10" style="border: 1px dotted #fff; !important ">';
//         $html.='<tr> <th> مسلسل</th> <th> اسم الموظف</th> <th> رقم الجواز</th> <th> رقم التأشيرة</th> <th colspan="2"> خط سير الرحلة</th> </tr>';
//         $html.=' <tr>
//         <th style="font-size:14px !important; border:none !important;" ></th>
//         <th style="font-size:14px !important; border:none !important;" ></th>
//         <th style="font-size:14px !important; border:none !important;" ></th>
//         <th style="font-size:14px !important; border:none !important;" ></th>
//         <th style="font-size:14px !important; border:none !important;" >من</th>
//         <th style="font-size:14px !important; " >الى</th>
//          <th style="font-size:14px !important; border:none !important;" ></th>

//     </tr>';


//         for($i=0; $i<count($requests['details']);$i++){



//             $html.='<tr><td  style="border: 0px dotted #000;">'. strval($i+1) .'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->employeeName.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->passNumber.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->visaNumber.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->from.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->to.'</td> </tr>';

//         }


//         $html.='</table>';


//         $html.='<br>';
//         $html.='<br>';
//         $html.='<p> المقيم : '.$responder->firstName.' '. $responder->middleName.' '.$responder->lastName.'</p>';

//         // $html.='<br>';
//         // $html.='<br>';


//         // $html.='<table cellpadding="10">';

//         // $html.='<tr><td> توقيع العميل</td> <td></td> <td></td> <td></td> <td> توقيع المستثمر</td></tr>';

//         // $html.='<tr><td> - - - - - - - - - -  </td> <td></td> <td></td> <td></td> <td>- - - - - - - - - - -</td></tr>';

//         // $html.='</table>';

// // create new PDF document
//         $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// // set default header data
//         // $pdf->SetHeaderData('amen_logo.png', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
//       //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// // set header and footer fonts
//         $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//         $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// // set default monospaced font
//         $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// // set margins
//         $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//         $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//         $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// // set auto page breaks
//         $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// // set image scale factor
//         $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// // set some language dependent data:
//         $lg = Array();
//         $lg['a_meta_charset'] = 'UTF-8';
//         $lg['a_meta_dir'] = 'rtl';
//         $lg['a_meta_language'] = 'fa';
//         $lg['w_page'] = 'page';

// // set some language-dependent strings (optional)
//         $pdf->setLanguageArray($lg);

// // ---------------------------------------------------------
// // set font
//         $pdf->SetFont('dejavusans', '', 12);
// // add a page
//         $pdf->AddPage();
//         $pdf->WriteHTML($html, true, 0, true, 0);
// // set LTR direction for english translation
//         $pdf->setRTL(false);
//         $pdf->SetFontSize(10);
// // print newline
//         $pdf->Ln();
// //Close and output PDF document
//         $pdf->Output("Contract_ar".$id.".pdf",'D');
//         $pdf->Output("C:\\xampp\\htdocs\\ta2shera\\public\\PDF\\Contract_ar".$id.".pdf",'F');
// //============================================================+
// // END OF FILE
// //============================================================+
    }




    public function set_final($id, Request $request){

        //dd($request->all());
        $requests = Requests::find($id);
        $requests->status='accepted';
        $requests->accept_number= $request->accept_number;
        if($request->comment){
            $requests->certifier_comment= $request->comment;
        }
        if($request->accept_date){
            $requests->accept_number_date= $request->accept_date;
        }

        //$requests->responser = auth()->user()->id;
        $requests->certifier = auth()->user()->id;
        $requests->save();


        $this->email = $requests->email;
        $this->name = $requests->firstName . ' ' . $requests->lastName;
        $requests['details']= unserialize($requests->details);
        $from = User::find($requests->from);
        $data = array('name'=>$this->name,'id'=>$requests->id,'status'=>$requests->status);
        Mail::send('mail', $data, function($message) {
            $message->to( $this->email , $this->name)->subject
               ('خدمة استثناء خروج وعودة');
            $message->from('ihd@mim.gov.sa','خدمة استثناء خروج وعودة');
         });


         $responder = null;
         if($requests->responser){
             $responder=User::find($requests->responser);

         }
         $employee_text='';
         $employee_text_2='';
         $employee_text_3='';
          if(count($requests['details'])==1){
             $employee_text = 'الموظف الحاصل';
             $employee_text_2 = 'بياناته';
             $employee_text_3 = 'دخوله';
          }else if(count($requests['details'])>1){
             $employee_text = 'الموظفين الحاصلين';
             $employee_text_2 = 'بياناتهم';
             $employee_text_3 = 'دخولهم';
          }
         $html='<style>
         td {border: 0px dotted #fff; }
         th {border: 1px dotted #000; }
         </style>';

         // $html.='<table cellpadding="10">';
         // $html.='<tr style="direction:ltr !important;"> <td  style="border: 0px dotted #000;"> </td> <td><img src="/tamken/public/assets/img/header.jpg" style="width:200;" /> </td> <td> </td></tr>';
         // $html.='</table>';

         // $html.='<img class="img" src="/tamken/public/assets/img/header.jpg"/>';


         $html.='<p class="am-label" style="text-align:left; width:50px;font-size:13px;" >الموضوع: بشأن طلب تسهيل إجراءات دخول
         موظف</p>';
         $html.='<p class="am-label" style="text-align:left">'.$requests->factory_name.'</p>';
         $html.='<p class="am-label" style="text-align:center">برقية سرية وعاجلة جدًا</p>';

         $html.='<table cellpadding="10">';
         $html.='<tr><td colspan="2">معالي مدير عام الجوازات	</td><td></td> <td></td><td>       حفظه الــله</td></tr>';
         $html.='<tr><td colspan="2">السلام عليكم ورحمة الــله وبركاته</td><td></td> <td></td></tr>';
         $html.='</table>';


         $html.='<br>';

         $html.='<p class="am-label" >
         إشارة إلى برقية صاحب السمو الملكي وزير الداخلية رقم (150679) وتاريخ 12/7/1442ه، المشار فيها إلى برقية معالي رئيس اللجنة التنفيذية الأمنية رقم (296) وتاريخ 10/7/1442ه، المتضمنة رأي اللجنة التنفيذية الأمنية مناسبة ما رآه معالي وزير الصحة في البرقية رقم (1223682) وتاريخ 1/7/1442ه، بشأن الموافقة على دخول المهندسين والفنيين، من رعايا بعض الدول المشمولة في قرار المنع من دخول المملكة بسبب جائحة (كورونا)، وإشارة إلى التعهد والالتزام المقدم من '.$requests->factory_name.' لإجراءات الاحترازية للحد من تفشي فايروس (كورونا) (مرفق بكامل مرفقاته)، الخاص بطلب عودة '.$employee_text.' على تأشيرة خروج وعودة، الموضحة '.$employee_text_2.' في الجدول أدناه.

         </p>';
         $html.='<br>';
         $html.='<br>';


         $html.='<br>';
         $html.='<br>';
         $html.='<br>';

         $html.='<table cellpadding="10" style="border: 1px dotted #fff; !important ">';
         $html.='<tr> <th> مسلسل</th> <th> اسم الموظف</th> <th> رقم الجواز</th> <th> رقم التأشيرة</th> <th colspan="2"> خط سير العمل</th> </tr>';
         $html.=' <tr>
         <th style="font-size:14px !important; border:none !important; border-right:1px solid #000;" ></th>
         <th style="font-size:14px !important; border:none !important;" ></th>
         <th style="font-size:14px !important; border:none !important;" ></th>
         <th style="font-size:14px !important; border:none !important;" ></th>
         <th style="font-size:14px !important; " >من</th>
         <th style="font-size:14px !important; " >الى</th>
          <th style="font-size:14px !important; border:none !important;" ></th>

     </tr>';


         for($i=0; $i<count($requests['details']);$i++){



             $html.='<tr><td  style="border: 0px dotted #000;">'. strval($i+1) .'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->employeeName.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->passNumber.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->visaNumber.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->from.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->to.'</td> </tr>';

         }


         $html.='</table>';


         $html.='<br>';
         $html.='<br>';

         $html.='<p class="am-label" style="line-height:30px;" >
         آمل من معاليكم تسهيل إجراءات '.$employee_text_3.'، ويمكن التنسيق في ذلك مع سعادة الأستاذ/ ماجد بن صالح الصيخان، على الجوال رقم (0544887750)، أو البريد الإلكتروني (sa.gov.mim@saikhan.majed).
         </p>';

         $html.='<br>';
         $html.='<br>';

         $html.='<p class="am-label" style="text-align:center" >
         وتقبلوا معاليكم خالص تحياتي
         </p>';

         $html.='<p class="am-label" style="text-align:left" >
         نائب وزير الصناعة والثروة المعدنية
         </p>';
         $html.='<br>';
         $html.='<br>';
         $html.='<br>';
         $html.='<br>';
         $html.='<br>';
         $html.='<br>';
         $html.='<p class="am-label" style="text-align:left" >
         م. أسامة بن عبد العزيز الزامل
         </p>';




         // $html.='<br>';
         // $html.='<br>';
        // $html.='<table cellpadding="10">';

        // $html.='<tr><td> توقيع العميل</td> <td></td> <td></td> <td></td> <td> توقيع المستثمر</td></tr>';

        // $html.='<tr><td> - - - - - - - - - -  </td> <td></td> <td></td> <td></td> <td>- - - - - - - - - - -</td></tr>';

        // $html.='</table>';

// create new PDF document
        $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set default header data
        // $pdf->SetHeaderData('amen_logo.png', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
      //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
        $lg = Array();
        $lg['a_meta_charset'] = 'UTF-8';
        $lg['a_meta_dir'] = 'rtl';
        $lg['a_meta_language'] = 'fa';
        $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
        $pdf->setLanguageArray($lg);

// ---------------------------------------------------------
// set font
        $pdf->SetFont('dejavusans', '', 12);
// add a page
        $pdf->AddPage();
        $pdf->WriteHTML($html, true, 0, true, 0);
// set LTR direction for english translation
        $pdf->setRTL(false);
        $pdf->SetFontSize(10);
// print newline
        $pdf->Ln();
//Close and output PDF document
        $pdf->Output("Contract_ar".$id.".pdf",'D');
    //   $pdf->Output("C:\\xampp\\htdocs\\ta2shera\\public\\PDF\\Contract_ar".$id.".pdf",'F');
//============================================================+
// END OF FILE
//============================================================+
    }


    public function set_returned($id, Request $request){



        $requests = Requests::find($id);
        $requests->status='returned';
        $requests->response_comments= $request->comment;
        $requests->responser = auth()->user()->id;
        $requests->save();


        $this->email = $requests->email;
        $this->name = $requests->firstName . ' ' . $requests->lastName;
        $requests['details']= unserialize($requests->details);
        $from = User::find($requests->from);
        $data = array('name'=>$this->name,'id'=>$requests->id,'status'=>$requests->status);
        Mail::send('mail', $data, function($message) {
            $message->to( $this->email , $this->name)->subject
               ('خدمة استثناء خروج وعودة');
            $message->from('ihd@mim.gov.sa','خدمة استثناء خروج وعودة');
         });


         $responder = null;
         if($requests->responser){
             $responder=User::find($requests->responser);

         }

//         $html='<style>
//         td {border: 0px dotted #fff; }
//         th {border: 1px dotted #000; }
//         </style>';

//         // $html.='<table cellpadding="10">';
//         // $html.='<tr style="direction:ltr !important;"> <td  style="border: 0px dotted #000;"> </td> <td><img src="/tamken/public/assets/img/header.jpg" style="width:200;" /> </td> <td> </td></tr>';
//         // $html.='</table>';

//         // $html.='<img class="img" src="/tamken/public/assets/img/header.jpg"/>';


//         $html.='<p class="am-label" style="text-align:center">طلب رقم '.$id.'</p>';
//         $html.='<table cellpadding="10">';
//         $html.='<tr><td><p style=" text-decoration: underline;"> بيانات المتقدم </p></td><td></td> </tr>';

//         $html.='<tr><td>الاسم : '. $from->firstName .' '. $from->middleName . ' '. $from->lastName . '</td><td></td><td></td></tr>';
//         $html.='<tr><td>البريد الالكتروني :'. $from->email .'</td><td></td><td></td></tr>';
//         $html.='<tr><td>الهاتف : '. $from->phone .'</td><td></td><td></td></tr>';
//         $html.='</table>';


//         $html.='<br>';
//         $html.='<br>';
//         $html.='<br>';
//         $html.='<tr><td><p style=" text-decoration: underline;"> بيانات الطلب </p></td><td></td> </tr>';

//         $html.='<br>';
//         $html.='<br>';

//         $html.='<table cellpadding="10" style="border: 1px dotted #fff; !important ">';
//         $html.='<tr> <th> مسلسل</th> <th> اسم الموظف</th> <th> رقم الجواز</th> <th> رقم التأشيرة</th> <th colspan="2"> خط سير الرحلة</th> </tr>';
//         $html.=' <tr>
//         <th style="font-size:14px !important; border:none !important;" ></th>
//         <th style="font-size:14px !important; border:none !important;" ></th>
//         <th style="font-size:14px !important; border:none !important;" ></th>
//         <th style="font-size:14px !important; border:none !important;" ></th>
//         <th style="font-size:14px !important; border:none !important;" >من</th>
//         <th style="font-size:14px !important; " >الى</th>
//          <th style="font-size:14px !important; border:none !important;" ></th>

//     </tr>';


//         for($i=0; $i<count($requests['details']);$i++){



//             $html.='<tr><td  style="border: 0px dotted #000;">'. strval($i+1) .'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->employeeName.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->passNumber.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->visaNumber.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->from.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->to.'</td> </tr>';

//         }


//         $html.='</table>';


//         $html.='<br>';
//         $html.='<br>';
//         $html.='<p> المقيم : '.$responder->firstName.' '. $responder->middleName.' '.$responder->lastName.'</p>';

//         // $html.='<br>';
//         // $html.='<br>';


//         // $html.='<table cellpadding="10">';

//         // $html.='<tr><td> توقيع العميل</td> <td></td> <td></td> <td></td> <td> توقيع المستثمر</td></tr>';

//         // $html.='<tr><td> - - - - - - - - - -  </td> <td></td> <td></td> <td></td> <td>- - - - - - - - - - -</td></tr>';

//         // $html.='</table>';

// // create new PDF document
//         $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// // set default header data
//         // $pdf->SetHeaderData('amen_logo.png', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
//       //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// // set header and footer fonts
//         $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//         $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// // set default monospaced font
//         $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// // set margins
//         $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//         $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//         $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// // set auto page breaks
//         $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// // set image scale factor
//         $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// // set some language dependent data:
//         $lg = Array();
//         $lg['a_meta_charset'] = 'UTF-8';
//         $lg['a_meta_dir'] = 'rtl';
//         $lg['a_meta_language'] = 'fa';
//         $lg['w_page'] = 'page';

// // set some language-dependent strings (optional)
//         $pdf->setLanguageArray($lg);

// // ---------------------------------------------------------
// // set font
//         $pdf->SetFont('dejavusans', '', 12);
// // add a page
//         $pdf->AddPage();
//         $pdf->WriteHTML($html, true, 0, true, 0);
// // set LTR direction for english translation
//         $pdf->setRTL(false);
//         $pdf->SetFontSize(10);
// // print newline
//         $pdf->Ln();
// //Close and output PDF document
//         $pdf->Output("Contract_ar".$id.".pdf",'D');
//         $pdf->Output("C:\\xampp\\htdocs\\ta2shera\\public\\PDF\\Contract_ar".$id.".pdf",'F');
// //============================================================+
// END OF FILE
//============================================================+
    }


    public function print_ar($id){


        $requests = Requests::find($id);
        // $requests->status='processed';
        // $requests->response_comments= $request->comment;
        // $requests->responser = auth()->user()->id;
        // $requests->save();
            $responder = null;
            if(isset($requests->responser)){
                $responder=User::find($requests->responser);

            }
        $this->email = $requests->email;
        $this->name = $requests->firstName . ' ' . $requests->lastName;
        $requests['details']= unserialize($requests->details);
        $from = User::find($requests->from);
        $data = array('name'=>$this->name,'id'=>$requests->id,'status'=>$requests->status);
        Mail::send('mail', $data, function($message) {
            $message->to( $this->email , $this->name)->subject
               ('خدمة استثناء خروج وعودة');
            $message->from('ihd@mim.gov.sa','خدمة استثناء خروج وعودة');
         });
        $employee_text='';
        $employee_text_2='';
        $employee_text_3='';
         if(count($requests['details'])==1){
            $employee_text = 'الموظف الحاصل';
            $employee_text_2 = 'بياناته';
            $employee_text_3 = 'دخوله';
         }else if(count($requests['details'])>1){
            $employee_text = 'الموظفين الحاصلين';
            $employee_text_2 = 'بياناتهم';
            $employee_text_3 = 'دخولهم';
         }
        $html='<style>
        td {border: 0px dotted #fff; }
        th {border: 1px dotted #000; }
        </style>';


        $html.='<p class="am-label" style="text-align:left; width:50px;font-size:13px;" >الموضوع: بشأن طلب تسهيل إجراءات دخول
        موظف</p>';
        $html.='<p class="am-label" style="text-align:left">'.$requests->factory_name.'</p>';

        $html.='';
        $html.='<p class="am-label" style="text-align:center">برقية سرية وعاجلة جدًا</p>';

        $html.='<br>';
        $html.='<br>';

        $html.='<table cellpadding="10">';
     
        $html.='<tr><td colspan="2">معالي مدير عام الجوازات	</td><td></td> <td></td><td>       حفظه الــله</td></tr>';
        $html.='<tr><td colspan="2">السلام عليكم ورحمة الــله وبركاته</td><td></td> <td></td></tr>';
        $html.='</table>';


        $html.='<br>';
        

        $html.='<p class="am-label" >
        إشارة إلى برقية صاحب السمو الملكي وزير الداخلية رقم (150679) وتاريخ 12/7/1442ه، المشار فيها إلى برقية معالي رئيس اللجنة التنفيذية الأمنية رقم (296) وتاريخ 10/7/1442ه، المتضمنة رأي اللجنة التنفيذية الأمنية مناسبة ما رآه معالي وزير الصحة في البرقية رقم (1223682) وتاريخ 1/7/1442ه، بشأن الموافقة على دخول المهندسين والفنيين، من رعايا بعض الدول المشمولة في قرار المنع من دخول المملكة بسبب جائحة (كورونا)، وإشارة إلى التعهد والالتزام المقدم من '.$requests->factory_name.' لإجراءات الاحترازية للحد من تفشي فايروس (كورونا) (مرفق بكامل مرفقاته)، الخاص بطلب عودة '.$employee_text.' على تأشيرة خروج وعودة، الموضحة '.$employee_text_2.' في الجدول أدناه.

        </p>';
        $html.='<br>';
        $html.='<br>';


        $html.='<br>';
        $html.='<br>';
        $html.='<br>';

        $html.='<table cellpadding="10" style="border: 1px dotted #fff; !important ">';
        $html.='<tr> <th> مسلسل</th> <th> اسم الموظف</th> <th> رقم الجواز</th> <th> رقم التأشيرة</th> <th colspan="2"> خط سير العمل</th> </tr>';
        $html.=' <tr>
        <th style="font-size:14px !important; border:none !important; border-right:1px solid #000;" ></th>
        <th style="font-size:14px !important; border:none !important;" ></th>
        <th style="font-size:14px !important; border:none !important;" ></th>
        <th style="font-size:14px !important; border:none !important;" ></th>
        <th style="font-size:14px !important; " >من</th>
        <th style="font-size:14px !important; " >الى</th>
         <th style="font-size:14px !important; border:none !important;" ></th>

    </tr>';


        for($i=0; $i<count($requests['details']);$i++){



            $html.='<tr><td  style="border: 0px dotted #000;">'. strval($i+1) .'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->employeeName.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->passNumber.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->visaNumber.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->from.'</td><td  style="border: 0px dotted #000;">'.$requests['details'][$i]->to.'</td> </tr>';

        }


        $html.='</table>';


        $html.='<br>';
        $html.='<br>';
        



        $html.='<table cellpadding="10" style="border: 1px dotted #fff; !important;text-align:right; " nobr="true" >';
        $html.='<tr> <td colspan="3" style="text-align:right;line-height:40px;"> آمل من معاليكم تسهيل إجراءات '.$employee_text_3.'، ويمكن التنسيق في ذلك مع سعادة الأستاذ/ ماجد بن صالح الصيخان، على الجوال رقم (0544887750)، أو البريد الإلكتروني (sa.gov.mim@saikhan.majed). </td></tr>';
        $html.='<tr> <td> </td> <td> وتقبلوا معاليكم خالص تحياتي </td> <td></td></tr>';
        $html.='<tr> <td></td><td> </td> <td>م. أسامة بن عبد العزيز الزامل </td> </tr>';
        
        $html.='<tr> <td></td><td> </td> <td style="width:240px;"> نائب وزير الصناعة والثروة المعدنية </td> </tr>';
        
        $html.='</table>';



       

// create new PDF document
        $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set default header data
        // $pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, '', 'الموضوع: بشأن طلب تسهيل إجراءات دخول
        // موظف');
      //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
        $pdf->setHeaderFont(Array('dejavusans', '', 12));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
         $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

// set margins
        $pdf->SetMargins(10, 15, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
        $lg = Array();
        $lg['a_meta_charset'] = 'UTF-8';
        $lg['a_meta_dir'] = 'rtl';
        $lg['a_meta_language'] = 'fa';
        $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
        $pdf->setLanguageArray($lg);

// ---------------------------------------------------------
// set font
        $pdf->SetFont('dejavusans', '', 11);
// add a page
        $pdf->AddPage();
        $pdf->WriteHTML($html, true, 0, true, 0);
// set LTR direction for english translation
        $pdf->setRTL(false);
        $pdf->SetFontSize(10);
// print newline
        $pdf->Ln();
//Close and output PDF document
        $pdf->Output("Contract_ar".$id.".pdf",'I');
     //  $pdf->Output("Contract_ar".$id.".pdf",'D');

//============================================================+
// END OF FILE
//============================================================+
    }


    public function my_requests_count(){
        $data =array();
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        if($user->type == 'evaluator' || $user->type == 'super_evaluator'){
            $new = Requests::where('status','new')->count();
            $processed = Requests::where('status','accepted')->count();
            $processing = Requests::where('status','processing')->count();
            $refused = Requests::where('status','refused')->count();

            array_push($data,$processed);
            array_push($data,$processing);
            array_push($data,$new);
            array_push($data,$refused);
        }else{
            $new = Requests::where('status','new')->where('from',$user_id)->count();
            $processed = Requests::where('status','processed')->where('from',$user_id)->count();
            $processing = Requests::where('status','processing')->where('from',$user_id)->count();
            $refused = Requests::where('status','refused')->where('from',$user_id)->count();

            array_push($data,$processed);
            array_push($data,$processing);
            array_push($data,$new);
            array_push($data,$refused);

        }




        return $data;
    }


     public function exportStatistics(Request $request)
    {
       // return Excel::download(new StatisticsExport($request->all()), 'statistics.xlsx');
       // $requests = Requests::all();
       $new = 0;
       $processed = 0;
       $processing = 0;
       $returned = 0;
       $initial_acceptance = 0;
       $refused =0;
       $all = 0 ;
       $requests = Requests::where('factory_name','<>',null);


       $status = $request->input('status');
       $from = $request->input('from');
       $to = $request->input('to');

       $user_id = auth()->user()->id;
       $user = User::find($user_id);
     //  $all= Requests::where('status','<>',null)->count();



       if($user->type == 'evaluator'){



        if (isset($from)) {
            $all= Requests::where('status','<>',null)->whereDate('created_at', '>=', $from)->count();

         $new = Requests::where('status','new')->whereDate('created_at', '>=', $from)->count();
         $processed = Requests::where('status','accepted')->whereDate('created_at', '>=', $from)->count();
         $processing = Requests::where('status','processing')->whereDate('created_at', '>=', $from)->count();
         $returned = Requests::where('status','returned')->whereDate('created_at', '>=', $from)->count();
         $initial_acceptance = Requests::where('status','initial_acceptance')->whereDate('created_at', '>=', $from)->count();
         $refused = Requests::where('status','refused')->whereDate('created_at', '>=', $from)->count();
     }elseif (isset($to)) {
        $all= Requests::where('status','<>',null)->whereDate('created_at', '<=', $to)->count();

         $new = Requests::where('status','new')->whereDate('created_at', '<=', $to)->count();
        $processed = Requests::where('status','accepted')->whereDate('created_at', '<=', $to)->count();
        $processing = Requests::where('status','processing')->whereDate('created_at', '<=', $to)->count();
        $returned = Requests::where('status','returned')->whereDate('created_at', '<=', $to)->count();
        $initial_acceptance = Requests::where('status','initial_acceptance')->whereDate('created_at', '<=', $to)->count();
        $refused = Requests::where('status','refused')->whereDate('created_at', '<=', $to)->count();
     }else{
        $all = Requests::count();
        $new = Requests::where('status','new')->count();
        $processed = Requests::where('status','accepted')->count();
        $processing = Requests::where('status','processing')->count();
        $returned = Requests::where('status','returned')->count();
        $initial_acceptance = Requests::where('status','initial_acceptance')->count();
        $refused = Requests::where('status','refused')->count();

     }




    }else{
        $all= Requests::count();
        $new = Requests::where('status','new')->where('from',$user_id)->count();
        $processed = Requests::where('status','accepted')->where('from',$user_id)->count();
        $processing = Requests::where('status','processing')->where('from',$user_id)->count();
        $returned = Requests::where('status','returned')->where('from',$user_id)->count();
        $initial_acceptance = Requests::where('status','initial_acceptance')->where('from',$user_id)->count();
        $refused = Requests::where('status','refused')->where('from',$user_id)->count();

    }
//dd($request->all());

       if (isset($status)) {
           $requests =  $requests->where("status", $status);
       }

       if (isset($from)) {
        $requests =  $requests->whereDate('created_at', '>=', $from);
    }
    if (isset($to)) {
        $requests =  $requests->whereDate('created_at', '<=', $to);
    }




    if($user->type == 'evaluator'){
    $requests= $requests->with('req_city')->get();
    }else{

        $requests= $requests->where('from',$user_id)->get();
    }







// create new PDF document
        $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set default header data
        // $pdf->SetHeaderData('amen_logo.png', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
      //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
        $lg = Array();
        $lg['a_meta_charset'] = 'UTF-8';
        $lg['a_meta_dir'] = 'rtl';
        $lg['a_meta_language'] = 'fa';
        $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
        $pdf->setLanguageArray($lg);

// ---------------------------------------------------------
// set font
        $pdf->SetFont('dejavusans', '', 12);
// add a page
$html='<style>
    td {border: 0px dotted #fff; }
    th {border: 1px dotted #000; }
    </style>';
    $html.='<div>';
    $html.='<h1 style="text-align:center;" > احصائيات طلبات خدمة الاستثناء </h1>';
    $html.='<h1 style="text-align:center;" >   الفترة من : '.$from .'    الى '.$to.' </h1>';
    $html.=' <table cellpadding="10" style="border: 1px dotted #fff; !important ">';
    $html.=' <tr>

    <th style="font-size:14px !important; " >الحالة</th>
    <th style="font-size:14px !important; " >العدد</th>



</tr>';
if (isset($status)  ) {
    if($status =='new'){
        $html.=' <tr>

        <td style="font-size:14px !important;border: 0px dotted #000; " >الاجمالي</td>

        <td style="font-size:14px !important; border: 0px dotted #000;" >'.$all.'</td>

        </tr>';
        $html.=' <tr>

    <td style="font-size:14px !important;border: 0px dotted #000; " >جديد</td>

    <td style="font-size:14px !important; border: 0px dotted #000;" >'.$new.'</td>

    </tr>';

    }elseif($status =='processing'){
        $html.=' <tr>

        <td style="font-size:14px !important;border: 0px dotted #000; " >الاجمالي</td>

        <td style="font-size:14px !important; border: 0px dotted #000;" >'.$all.'</td>

        </tr>';
        $html.=' <tr>

    <td style="font-size:14px !important;border: 0px dotted #000; " >تحت المعالجة</td>

    <td style="font-size:14px !important; border: 0px dotted #000;" >'.$processing.'</td>

    </tr>';

    }elseif($status =='processed'){
        $html.=' <tr>

        <td style="font-size:14px !important;border: 0px dotted #000; " >الاجمالي</td>

        <td style="font-size:14px !important; border: 0px dotted #000;" >'.$all.'</td>

        </tr>';
        $html.=' <tr>

    <td style="font-size:14px !important;border: 0px dotted #000; " >مقبول</td>

    <td style="font-size:14px !important; border: 0px dotted #000;" >'.$processed.'</td>

    </tr>';

    }elseif($status =='refused'){
        $html.=' <tr>

        <td style="font-size:14px !important;border: 0px dotted #000; " >الاجمالي</td>

        <td style="font-size:14px !important; border: 0px dotted #000;" >'.$all.'</td>

        </tr>';
        $html.=' <tr>

    <td style="font-size:14px !important;border: 0px dotted #000; " >مرفوض</td>

    <td style="font-size:14px !important; border: 0px dotted #000;" >'.$refused.'</td>

    </tr>';

    }elseif($status =='returned'){
        $html.=' <tr>

        <td style="font-size:14px !important;border: 0px dotted #000; " >الاجمالي</td>

        <td style="font-size:14px !important; border: 0px dotted #000;" >'.$returned.'</td>

        </tr>';
        $html.=' <tr>

    <td style="font-size:14px !important;border: 0px dotted #000; " >معاد</td>

    <td style="font-size:14px !important; border: 0px dotted #000;" >'.$new.'</td>

    </tr>';

    }elseif($status =='initial_acceptance'){
        $html.=' <tr>

        <td style="font-size:14px !important;border: 0px dotted #000; " >الاجمالي</td>

        <td style="font-size:14px !important; border: 0px dotted #000;" >'.$all.'</td>

        </tr>';
        $html.=' <tr>

    <td style="font-size:14px !important;border: 0px dotted #000; " >موافقة مبدئية</td>

    <td style="font-size:14px !important; border: 0px dotted #000;" >'.$initial_acceptance.'</td>

    </tr>';

    }

else{
    $html.=' <tr>

<td style="font-size:14px !important;border: 0px dotted #000; " >الاجمالي</td>

<td style="font-size:14px !important; border: 0px dotted #000;" >'.$all.'</td>

</tr>';

$html.=' <tr>

<td style="font-size:14px !important;border: 0px dotted #000; " >جديد</td>

<td style="font-size:14px !important; border: 0px dotted #000;" >'.$new.'</td>

</tr>';
$html.=' <tr>

<td style="font-size:14px !important;border: 0px dotted #000; " >تحت المعالجة</td>

<td style="font-size:14px !important; border: 0px dotted #000;" >'.$processing.'</td>

</tr>';
$html.=' <tr>

<td style="font-size:14px !important;border: 0px dotted #000; " >موافقة مبدئية </td>

<td style="font-size:14px !important; border: 0px dotted #000;" >'.$initial_acceptance.'</td>

</tr>';
$html.=' <tr>

<td style="font-size:14px !important;border: 0px dotted #000; " >مقبول</td>

<td style="font-size:14px !important; border: 0px dotted #000;" >'.$processed.'</td>

</tr>';
$html.=' <tr>

<td style="font-size:14px !important;border: 0px dotted #000; " >معاد</td>

<td style="font-size:14px !important;border: 0px dotted #000; " >'.$returned.'</td>

</tr>';
$html.=' <tr>

<td style="font-size:14px !important;border: 0px dotted #000; " >مرفوض</td>

<td style="font-size:14px !important; border: 0px dotted #000;" >'.$refused.'</td>

</tr>';

}
}else{
    $html.=' <tr>

    <td style="font-size:14px !important;border: 0px dotted #000; " >الاجمالي</td>

    <td style="font-size:14px !important; border: 0px dotted #000;" >'.$all.'</td>

    </tr>';

    $html.=' <tr>

    <td style="font-size:14px !important;border: 0px dotted #000; " >جديد</td>

    <td style="font-size:14px !important; border: 0px dotted #000;" >'.$new.'</td>

    </tr>';
    $html.=' <tr>

    <td style="font-size:14px !important;border: 0px dotted #000; " >تحت المعالجة</td>

    <td style="font-size:14px !important; border: 0px dotted #000;" >'.$processing.'</td>

    </tr>';
    $html.=' <tr>

    <td style="font-size:14px !important;border: 0px dotted #000; " >موافقة مبدئية </td>

    <td style="font-size:14px !important; border: 0px dotted #000;" >'.$initial_acceptance.'</td>

    </tr>';
    $html.=' <tr>

    <td style="font-size:14px !important;border: 0px dotted #000; " >مقبول</td>

    <td style="font-size:14px !important; border: 0px dotted #000;" >'.$processed.'</td>

    </tr>';
    $html.=' <tr>

    <td style="font-size:14px !important;border: 0px dotted #000; " >معاد</td>

    <td style="font-size:14px !important;border: 0px dotted #000; " >'.$returned.'</td>

    </tr>';
    $html.=' <tr>

    <td style="font-size:14px !important;border: 0px dotted #000; " >مرفوض</td>

    <td style="font-size:14px !important; border: 0px dotted #000;" >'.$refused.'</td>

    </tr>';
}


$html.=' </table>';
$html.=' </div>';

$pdf->AddPage();
$pdf->WriteHTML($html, true, 0, true, 0);

$page=1;

foreach($requests as $requestt){
$page+=1;

$status = '';
if($requestt->status=='new'){
    $status ='جديد';
}elseif($requestt->status=='accepted'){
    $status ='مقبول';
}elseif($requestt->status=='refused'){
    $status ='مرفوض';
}elseif($requestt->status=='returned'){
    $status ='معاد';
}elseif($requestt->status=='initial_acceptance'){
    $status ='موافقة مبدئية';
}elseif($requestt->status=='processing'){
    $status ='تحت المعالجة';
}



    $this->name = $requestt->firstName . ' ' . $requestt->lastName;

    $details= unserialize($requestt->details);
    $html='<style>
    td {border: 0px dotted #fff; }
    th {border: 1px dotted #000; }
    </style>';
    $html.='<div>';
    $html.='<p> رقم الطلب :'.$requestt->request_id.' </p>';
    $html.='<p>  الدولة :'.$requestt->req_city->name_ar.' </p>';
    $html.='<p>  رقم السجل التجاري :'.$requestt->cr.' </p>';
    $html.='<p>  رقم الترخيص الصناعي :'.$requestt->ir.' </p>';
    $html.='<p>  الاسم :'. $this->name.' </p>';
    $html.='<p>  البريد الالكتروني :'.$requestt->email.' </p>';
    $html.='<p>  اسم المصنع :'.$requestt->factory_name.' </p>';
    $html.='<p>  رمز المصنع  :'.$requestt->factory_symbol.' </p>';
    $html.='<table cellpadding="10" style="border: 1px dotted #fff; !important ">';
    $html.='<tr> <th> مسلسل</th> <th> اسم الموظف</th> <th> رقم الجواز</th> <th> رقم التأشيرة</th> <th colspan="2"> خط سير العمل</th> </tr>';
    $html.=' <tr>
    <th style="font-size:14px !important; border:none !important; border-right:1px solid #000;" ></th>
    <th style="font-size:14px !important; border:none !important;" ></th>
    <th style="font-size:14px !important; border:none !important;" ></th>
    <th style="font-size:14px !important; border:none !important;" ></th>
    <th style="font-size:14px !important; " >من</th>
    <th style="font-size:14px !important; " >الى</th>
     <th style="font-size:14px !important; border:none !important;" ></th>


</tr>';



for($i=0; $i<count($details);$i++){



    $html.='<tr><td  style="border: 0px dotted #000;">'. strval($i+1) .'</td><td  style="border: 0px dotted #000;">'.$details[$i]->employeeName.'</td><td  style="border: 0px dotted #000;">'.$details[$i]->passNumber.'</td><td  style="border: 0px dotted #000;">'.$details[$i]->visaNumber.'</td><td  style="border: 0px dotted #000;">'.$details[$i]->from.'</td><td  style="border: 0px dotted #000;">'.$details[$i]->to.'</td> </tr>';

}

    $html.='</table>';

    $html.='<p>  الحالة :'.$status.' </p>';
    $html.='<p>  تعليق المقيم :'.$requestt->response_comments.' </p>';
    $html.='<p>    رقم الصادر :'.$requestt->accept_number.' </p>';


    $html.='</div>';
    $pdf->AddPage();
    $pdf->WriteHTML($html, true, 0, true, 0);



}




$pdf->setRTL(false);
$pdf->SetFontSize(10);
// print newline
$pdf->Ln();
// set LTR direction for english translation

//Close and output PDF document
        $pdf->Output("statistis.pdf",'I');
     //  $pdf->Output("Contract_ar".$id.".pdf",'D');

    }

    public function delete_requestt($id)
    {
        $request = Requests::find($id);
        $request->delete();
        dd("deleted");
    }
}
