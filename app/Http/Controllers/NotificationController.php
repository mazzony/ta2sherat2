<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Illuminate\Http\Request;
use App\Models\User;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function show(Notification $notification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function edit(Notification $notification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notification $notification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notification $notification)
    {
        //
    }

    public function add_notification($request){


        if($request['type']=='sector'){
            $users =User::where('sector',intval($request['destination_id']))->get();


            foreach($users as $user){
                $notification = new Notification();
                $notification->destination_id =$user->id;
                $notification->title = $request['title'];
                $notification->body = $request['body'];
                $notification->seen = 0;
                $notification->type =$request['type'] ;
                $notification->url = $request['url'];
                $notification->post_id = intval($request['url']);
                $notification->save();

            }
        }

        if($request['type']=='admin'){
            $users =User::where('type','system_user')->get();


            foreach($users as $user){
                $notification = new Notification();
                $notification->destination_id =$user->id;
                $notification->title = $request['title'];
                $notification->body = $request['body'];
                $notification->seen = 0;
                $notification->type =$request['type'] ;
                $notification->url = $request['type'];
                $notification->save();

            }
        }

        if($request['type']=='request'){
                $notification = new Notification();
                $notification->destination_id = $request['destination_id'];
                $notification->title = $request['title'];
                $notification->body = $request['body'];
                $notification->seen = 0;
                $notification->type =$request['type'] ;
                $notification->url = $request['url'];
                $notification->post_id = intval($request['url']);
                $notification->save();
        }

        if($request['type']=='request_status'){
            if( $request['title']=='Request Accepted'){
                $users =User::where('type','system_user')->get();


            foreach($users as $user){
                $notification = new Notification();
                $notification->destination_id =$user->id;
                $notification->title = $request['title'];
                $notification->body = $request['body'];
                $notification->seen = 0;
                $notification->type ='admin' ;
                $notification->url = $request['url'];
                $notification->post_id = intval($request['url']);
                $notification->save();

            }

            }
            $notification = new Notification();
            $notification->destination_id = $request['destination_id'];
            $notification->title = $request['title'];
            $notification->body = $request['body'];
            $notification->seen = 0;
            $notification->type =$request['type'] ;
            $notification->url = $request['url'];
            $notification->post_id = intval($request['url']);
            $notification->save();
    }


        // $notification = new Notification();
        // $notification->destination_id = $request->destination_id;
        // $notification->title = $request->title;
        // $notification->body = $request->body;
        // $notification->seen = 0;
        // $notification->type =$request->type ;
        // $notification->url = $request->url;
        // $notification->save();

    }

    public function get_all(){
        $id = auth()->user()->id;
       $notifications = Notification::where('destination_id',$id)->where('seen',0)->get();
       return $notifications;

    }

    public function seen($id)
    {
        $notification = Notification::find($id);
        $notification->seen=1;
        $notification->save();
    }
}
