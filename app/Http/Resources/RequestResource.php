<?php

namespace App\Http\Resources;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class RequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            's_id' => $this->s_id,
            'cr' => $this->cr,
            'ir' => $this->ir,
            'request_id'=> $this->request_id,
            'city'=> $this->city,
            'factory_name' => $this->factory_name,
            'factory_symbol' => $this->factory_symbol,
            'firstName' => $this->firstName,
            'middleName' => $this->middleName,
            'lastName' => $this->lastName,
            'phone' => $this->phone,
            'email' => $this->email,
            'details' => $this->details,
            'from' => $this->from,
            'firstName' => $this->firstName,
            'status' => $this->status,
            'response'=>$this->response,
            'responser'=>$this->responser,
            'certifier'=>$this->certifier,
            'certifier_comment'=>$this->certifier_comment,
            'accept_number_date'=>$this->accept_number_date,
            'response_comments'=>$this->response_comments,
            'attachments'=>$this->attachments,
            'accept_number'=>$this->accept_number,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,

        ];


    }
}
