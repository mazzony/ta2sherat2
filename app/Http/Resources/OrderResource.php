<?php

namespace App\Http\Resources;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::find($this->id);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'trader_id' => $this->trader_id,
            'sector_id' => $this->sector_id,
            'status' => $this->status,
            'details' => $this->details,
            'comments'=>$this->comments,
            'user' => $user,
            'trader' => $this->trader(),
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];


    }
}
