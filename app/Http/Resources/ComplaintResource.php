<?php

namespace App\Http\Resources;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       // $user = User::find($this->id);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'attachement' => $this->attachement,
            'response' => $this->response,
            'response_by' => $this->response_by,
            'from'=>$this->from,
            'complainer' => $this->complainer(),
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];



    }
}
