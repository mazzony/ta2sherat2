<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;
    protected $fillable = [
        'destination_id',
        'title',
        'body',
        'seen',
        'type',
        'url',
        'post_id'

    ];
}
