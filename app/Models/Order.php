<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'trader_id',
        'sector_id',
        'status',
        'details',
        'comments'

    ];

    public function sector()
    {
        return $this->hasOne(Sector::class,'id', 'sector_id');
    }
    public function trader()
    {
        return $this->hasOne(User::class,'id', 'trader_id');
    }
}
