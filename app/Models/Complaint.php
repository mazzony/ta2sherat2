<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'description',
        'attachement',
        'response',
        'response_by',
        'from',


    ];

    public function complainer()
    {
        return $this->hasOne(User::class,'id', 'from');
    }
}
