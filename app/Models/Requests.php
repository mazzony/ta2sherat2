<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelIdGenerator\IdGenerator;

class Requests extends Model
{
    use HasFactory;

    protected $fillable = [
        'cr',
        'ir',
        'request_id',
        'city',
        'factory_name',
        'factory_symbol',
        'firstName',
        'middleName',
        'lastName',
        'phone',
        'email',
        'details',
        'from',
        'status',
        'response',
        'responser',
        'response_comments',
        'attachments',
        'certifier',
        'certifier_comment',
        'accept_number',
        'accept_number_date'
    ];

    public function from()
    {
        return $this->hasOne(User::class,'id', 'from');
    }
    public function to()
    {
        return $this->hasOne(User::class,'id', 'to');
    }
    public function req_city()
    {
        return $this->hasOne(City::class,'id', 'city');
    }
    public function order()
    {
        return $this->hasOne(Order::class,'id', 'order_id');
    }
    public function sector()
    {
        return $this->hasOne(Sector::class,'id', 'sector_id');
    }
}
