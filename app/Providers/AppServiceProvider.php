<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if($this->app['session']->has('applocale')){
     $locale = $this->app['session']->get('applocale'); // This would be the same as session()->get(... and Session::get(...
    $this->app->setLocale($locale); // This would also equal app()->setLocale(... and App::setLocale(...
    }else{
        $locale = $this->app['session']->put('applocale','ar'); // This would be the same as session()->get(... and Session::get(...
        $locale = $this->app['session']->get('applocale');
        $this->app->setLocale($locale);

    }

         Schema::defaultStringLength(191);

    }
}
