@extends('layouts.app')

@section('content')
@php
if(session()->has('applocale')){
    if(session()->get('applocale')=='en')
     App::setLocale('en');
    else
    App::setLocale('ar');
}else {
    App::setLocale('ar');
}

@endphp
<div class="subheader min-h-lg-100px pt-5 pb-7 subheader-transparent" id="kt_subheader">
    <div class="container align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <!--begin::Details-->
        <div class="align-items-center flex-wrap mr-2 align-self-center">
            <!--begin::Heading-->
            <div class="d-flex flex-column" style="height: 150px;">
                <!--begin::Title-->
                <h2 class="text-white title-section my-2 mr-5" style="font-size:30px;    padding-top: 80px;
                padding-bottom: 30px;">{{__('lang.home')}}</h2>
                <!--end::Title-->
            </div>
            <!--end::Heading-->
        </div>
        <!--end::Details-->
    </div>
</div>
<div class="d-flex flex-column-fluid bg-battern">

    <!--begin::Container-->
    <div class="container">
    <div>




</div>                            <!--begin::Main Body-->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Card-->
                <div class="card card-custom gutter-b example example-compact">
                    <div class="card-body">
                        <div class="mim-text">
                                <div wire:id="sDkqgFtP220hmC89SezD" class="continer d-flex justify-content-center align-items-center" style="min-height: 350px;">
<div class="text-center">
<h2 class="h5 pt-0 pb-1 gold-color" style="text-align: center; font-size:25px !important;">
    {{__('lang.ministry_title')}}
</h2>
<h3 class="h2 py-2 " style="text-align: center; font-size:28px !important;">
    {{__('lang.welcoming')}}
</h3>
<p class="gray-color font-14" style="text-align: center;">
    {{__('lang.service_desc')}}
</p>
<div class="banner-btns pt-3" style="text-align: center;font-size:22px !important;">
    <a class="btn btn-blick- btn-main" href="login"  style="text-align: center;font-size:22px !important;">{{__('lang.Login')}}</a>
</div>
</div>
</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Main Body-->
    </div>
    <!--end::Container-->
</div>
@endsection
