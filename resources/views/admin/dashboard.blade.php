<!-- File: ./resources/views/admin/dashboard.blade.php -->
<!DOCTYPE html>

@if(session('applocale'))
@if(session('applocale') == 'ar')
<html lang="ar" dir="rtl">
@else
<html lang="en" dir="ltr">
@endif
@else
<html lang="ar" dir="rtl">
@endif
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="/ta2shera/public/assets/img/favicon.ico">
    <script> window.Laravel = { csrfToken: 'csrf_token() ' } </script>
    <title> Welcome to the dashboard </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <script src="{{ asset('js/lang.js') }}"></script>
    @if(session('applocale'))
    @if(session('applocale') == 'ar')
    <link rel="stylesheet" href="/ta2shera/public/css/rtl.css">
    @elseif (session('applocale') == 'en')
    <link rel="stylesheet" href="/ta2shera/public/css/ltr.css">
    @endif
    @else
    <link rel="stylesheet" href="/ta2shera/public/css/rtl.css">
    @endif
    <link href="/ta2shera/public/assets/css/animate/animate.min.css" rel="stylesheet">
    <link href="/ta2shera/public/assets/css/style.css" rel="stylesheet">
     <link href="/ta2shera/public/assets/css/responsive.css" rel="stylesheet">

</head>
<body>


    <div id="app">

        <Homepage
          :user-name='@json(auth()->user()->firstName)'
          :user-id='@json(auth()->user()->id)'
        > </Homepage>
      </div>

  <script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
