<!-- JQuery -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery-ui/jquery-ui.js"></script>
<!-- popper -->
<script src="assets/js/popper/popper.min.js"></script>
<script src="assets/js/popper/moment.min.js"></script>
<!-- bootstrap -->
<script src="assets/js/bootstrap/bootstrap.min.js"></script>
<!-- bootstrap-select -->
<script src="assets/js/bootstrap-select/bootstrap-select.min.js"></script>
<!-- bootstrap-date -->
<script src="assets/js/bootstrap-date/bootstrap-datetimepicker.min.js"></script>
<!-- OWL -->
<script src="assets/js/owl-carousel/owl.carousel.min.js"></script>
<!-- easing -->
<script src="assets/js/easing/jquery.easing.1.3.js"></script>
<!-- WOW -->
<script src="assets/js/wow/wow.min.js"></script>
<!-- equal height -->
<script src="assets/js/matchHeight/jquery.matchHeight-min.js"></script>
<!-- niceScroll -->
<script src="assets/js/niceScroll/jquery.nicescroll.min.js"></script>
<!-- parsley -->
<script src="assets/js/parsley/parsley.min.js"></script>
<!-- Toast -->
<script src="assets/js/toast/toast.min.js"></script>
<!-- Typed -->
<script src="assets/js/typed/typed.min.js"></script>
<!-- select-lib -->
<script src="assets/js/select-lib/jquery.selectric.min.js"></script>
<!-- wizard -->
<script src="assets/js/wizard/jquery.steps.min.js"></script>
<!-- repeater -->
<script src="assets/js/repeater/jquery.repeater.min.js"></script>
<!-- dialog -->
<script src="assets/js/dialog/jquery-confirm.min.js"></script>
<!-- emoji -->
<script src="assets/js/emoji/emojionearea.min.js"></script>
<!-- popup image -->
<script src="assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
