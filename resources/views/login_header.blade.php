<header>
    <nav class="shadow navbar navbar-expand-xl bg-white navbar-light fixed-top animated slideInDown">
        <div class="container-fluid">
            <a href="/" class="navbar-brand">
                <img src="{{$logo_product}}" height="50"/>
            </a>

            <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <div class="position-relative">
                                <a href="https://mim.gov.sa/" class="btn btn-global btn-border-green btn-green-color shadow-sm">
                                    <i class="fa fa-home pl-2 pr-2"></i>
                                    {{__('lang.home')}}
                                </a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <div class="position-relative">
                                <a href="register" class="btn btn-global btn-border-green btn-green-color shadow-sm">{{__('lang.Register')}}</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <div class="position-relative">
                                @if(session()->has('applocale'))
                                @if(session()->get('applocale')=='ar')
                                <a href="#" class="btn-divider-right shadow-sm btn toggle_language_site" onclick="lang_switch()">
                                    <span class="btn_text" id="switcher">EN</span>
                                </a>

                                @else
                                <a href="#" class="btn-divider-right shadow-sm btn toggle_language_site" onclick="lang_switch()">
                                    <span class="btn_text" id="switcher">عربي</span>
                                </a>
                                @endif
                            @else
                            <a href="#" class="btn-divider-right shadow-sm btn toggle_language_site" onclick="lang_switch()">
                                <span class="btn_text" id="switcher">EN</span>
                            </a>

                            @endif
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>
