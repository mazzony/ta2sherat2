<header>
    <div class="site-header-wrapper pre-header">
                      <nav class="top-navbar navbar navbar-expand-lg d-block topbar topnav">
                          <div class="container-fluid">
                              <div class="row px-0 mx-0 w-100">
                                  <div class="col-md-6 d-flex justify-content-center justify-content-md-start align-items-center">
                                      <ul class="navbar-nav options-list">
                                          <li class="nav-item mx-auto"><a target="_blank" href="https://vision2030.gov.sa/" class="nav-link py-1"><img height="45" src="https://inquiries.mim.gov.sa/mim/assets/media/logos/vision_wh.png"></a></li>
                                          <li class="nav-item mauto">
                                              <a class="nav-link" target="_blank" href="tel:199001">
                                                  <i class="fa fa-phone-square txt-mimgold mx-1 f-20"></i><span class="txt-mimgold" style="font-size:16px;">199001</span>
                                              </a>
                                          </li>
                                          <li class="nav-item mauto">
                                              <a class="nav-link" target="_blank" href="https://twitter.com/mimgov?s=09">
                                                  <i class="fa fa-twitter text-white mx-1 f-20"></i>
                                              </a>
                                          </li>
                                          <li class="nav-item mauto">
                                              <a class="nav-link" target="_blank" href="#">
                                                  <i class="fa fa-linkedin-square text-white mx-1 f-20"></i>
                                              </a>
                                          </li>
                                      </ul>
                                  </div>
                                  <div class="col-md-6 d-flex justify-content-center justify-content-md-end align-items-center">
                                      <ul class="navbar-nav options-list">
                                          <li class="nav-item mx-2"><a class="nav-link" href="https://mim.gov.sa/mim_usage_disclaimer" style="font-size:14px;"> {{__('lang.disclaimer')}}</a></li>


                                          <!-- <li class="nav-item mx-2">
                                              <a class="nav-link" href="http://www.mim.gov.sa/">تواصل معنا</a>
                                          </li> -->
                                          <li class="nav-item mx-2">
                                            @if(session()->has('applocale'))
                                            @if(session()->get('applocale')=='ar')

                                            <a class="nav-link"  href="#" style="font-size:14px;line-height:1.5;" onclick="lang_switch()">
                                                <span class="text-white font-weight-normal"  id="switcher" >EN</span>
                                            </a>

                                            @else

                                            <a class="nav-link"  href="#" style="font-size:14px;line-height:1.5;" onclick="lang_switch()">
                                                <span class="text-white font-weight-normal"  id="switcher" >ع</span>
                                            </a>
                                            @endif
                                        @else

                                        <a class="nav-link"  href="#" style="font-size:14px;line-height:1.5;" onclick="lang_switch()">
                                            <span class="text-white font-weight-normal"  id="switcher" >EN</span>
                                        </a>

                                        @endif

                                          </li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </nav>
                  </div>
    </header>
    <nav id="navbar" ref="navbar">
   <div class="container d-flex align-items-stretch justify-content-between">
                          <!--begin::Left-->
                          <div class="d-flex align-items-stretch mr-3">
                              <!--begin::Header Logo-->
                              <div class="header-logo">
                                  <a href="">
                                      <img alt="Logo" src="/ta2shera/public/assets/img/logo_product_ar.png" class="logo-default max-h-70px">

                                  </a>
                              </div>
                              <!--end::Header Logo-->
                              <!--begin::Header Menu Wrapper-->
                              <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                                  <!--begin::Header Menu-->
                                  <div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile header-menu-layout-default">
                                      <!--begin::Header Nav-->
                                      <ul class="menu-nav">

                                      <!-- @if (Auth::user())
                                        
                                          <li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
                                              <a href="https://visa-approval.mim.gov.sa/" class="menu-link">
                                                  <span class="menu-text">{{__('lang.home')}}</span>
                                                  <i class="menu-arrow"></i>
                                              </a>
                                          </li>
                                          @else
                                          <li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
                                              <a href="/ta2shera/public" class="menu-link">
                                                  <span class="menu-text">{{__('lang.home')}}</span>
                                                  <i class="menu-arrow"></i>
                                              </a>
                                          </li>
                                          @endif -->
                                          <li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
                                              <a href="https://mim.gov.sa/mim/index.html" class="menu-link" _target="blank">
                                                  <span class="menu-text"> {{__('lang.ministry_site')}}</span>
                                                  <span class="menu-desc"></span>
                                                  <i class="menu-arrow"></i>
                                              </a>
                                          </li>
                                                                                  <!-- <li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
                                              <a href="https://inquiries.mim.gov.sa/index.php/ar/login" class="menu-link" _target="blank">
                                                  <span class="menu-text">تسجيل الدخول</span>
                                                  <span class="menu-desc"></span>
                                                  <i class="menu-arrow"></i>
                                              </a>
                                           </li> -->
                                                                              </ul>
                                      <!--end::Header Nav-->
                                  </div>
                                  <!--end::Header Menu-->
                              </div>
                              <!--end::Header Menu Wrapper-->
                          </div>
                          <!--end::Left-->
                          <!--begin::Topbar-->
                          <div class="topbar">
                              <!--begin::Notifications-->
                              <!--end::Notifications-->
                              <!--begin::User-->
                              <!--end::User-->
                          </div>
                          <!--end::Topbar-->
                      </div>
    </nav>
