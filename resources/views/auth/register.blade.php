@extends('layouts.app')

@section('content')
@php
if(session()->has('applocale')){
    if(session()->get('applocale')=='en')
     App::setLocale('en');
    else
    App::setLocale('ar');
}else {
    App::setLocale('ar');
}

@endphp
<section id="Home-Login" class="cd-section">

<div class="container">
    <div class="row">
        <style>
            .checkbox input[type="checkbox"]:checked + span,
            .radio input[type="radio"]:checked + span{
                background-color: #fff!important;
                color: #33414A !important;
                /* border-top-right-radius: 30px; */
                /* border-bottom-left-radius: 30px; */
                /* box-shadow: 5px 4px 2px #eee; */
                box-shadow: 0 0 30px 0 rgba(0,0,0,.1);
                border-bottom: 4px solid #1AD9C7;
            }
            *[dir="rtl"] .checkbox input[type="checkbox"]:checked + span.right,
            *[dir="rtl"] .radio input[type="radio"]:checked + span.right{
                border-top-left-radius: 50px !important;
            }
            *[dir="rtl"] .checkbox input[type="checkbox"]:checked + span.left,
            *[dir="rtl"] .radio input[type="radio"]:checked + span.left{
                border-top-right-radius: 50px !important;
            }
            *[dir="ltr"] .checkbox input[type="checkbox"]:checked + span.right,
            *[dir="ltr"] .radio input[type="radio"]:checked + span.right {
                border-top-right-radius: 50px !important;
            }
            *[dir="ltr"] .checkbox input[type="checkbox"]:checked + span.left,
            *[dir="ltr"] .radio input[type="radio"]:checked + span.left{
                border-top-left-radius: 50px !important;
            }
        </style>
        <div class="col-xl-10 mx-auto animated fadeInUp">



            <div class="register-login card shadow rounded">



                <div class="register-form-client-div">
                    <form  id="register-form-client" method="POST"  action="{{ route('register') }}">
                        @csrf
<div class="row">
    <div class="col-xl-4">
        <div>
            <label for="firstName" class="label-form d-block">{{ __('lang.firstName') }}</label>

            <div class="form-group text-right">
                <input id="firstName" type="text" class="form-control @error('firstName') is-invalid @enderror" name="firstName" value="{{ old('firstName') }}" required autocomplete="firstName" autofocus>

                <div class="icon-form-group">
                    <i class="fa fa-pen icon-input-form"></i>

                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-4">
        <div>
            <label for="middleName" class="label-form d-block">{{ __('lang.middleName') }}</label>

            <div class="form-group text-right">
                <input id="middleName" type="text" class="form-control @error('middleName') is-invalid @enderror" name="middleName" value="{{ old('middleName') }}" required autocomplete="middleName" autofocus>

                <div class="icon-form-group">
                    <i class="fa fa-pen icon-input-form"></i>

                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-4">
        <div>
            <label for="lastName" class="label-form d-block">{{ __('lang.lastName') }}</label>

            <div class="form-group text-right">
                <input id="lastName" type="text" class="form-control @error('lastName') is-invalid @enderror" name="lastName" value="{{ old('lastName') }}" required autocomplete="lastName" autofocus>

                <div class="icon-form-group">
                    <i class="fa fa-pen icon-input-form"></i>

                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-xl-6">
        <div>
            <label for="name" class="label-form d-block">{{ __('lang.E-Mail Address') }}</label>

            <div class="form-group text-right">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="icon-form-group">
                    <i class="fa fa-envelope icon-input-form"></i>

                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6">
        <div>
            <label for="phone" class="label-form d-block">{{ __('lang.phone') }}</label>

            <div class="form-group text-right">
                <input id="phone" type="phone" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone">

                @error('phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="icon-form-group">
                    <i class="fa fa-phone icon-input-form"></i>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-6">
        <div>
            <label for="name" class="label-form d-block">{{ __('lang.Password') }}</label>

            <div class="form-group text-right">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
                <div class="icon-form-group">
                    <i class="fa fa-lock icon-input-form"></i>

                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6">

        <div>
            <label for="name" class="label-form d-block">{{ __('lang.Confirm Password') }}</label>

            <div class="form-group text-right">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="icon-form-group">
                    <i class="fa fa-lock icon-input-form"></i>

                </div>
            </div>
        </div>
    </div>
</div>


                        <div>
                            <div class="text-center mt-5 mb-5">
                                <button type="submit" class="ml-3 btn btn-global btn-border-rounded-green btn-white-color bg-padding-btn bg-btn-green shadow" style="background-color: #1AD9C7;border-color:#1AD9C7;color:#262539;">
                                    {{ __('lang.Register') }}
                                </button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
</div>

</section>
<script>
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $('#sector').select2();
    $('#sector').select2({
        ajax: {
            url: '/api/sector/get_all_fe',
            dataType: 'json',
            type: "post",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              _token: CSRF_TOKEN,
              search: params.term // search term
            };
          },
            processResults: function (data) {
                console.log(data);
                return {
                    results: data
                };
                }
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        }
        });

        $('.register-form-investor-div').hide();
        $('#client_toggle_form').click(function () {
            $('.register-form-investor-div').hide();
            $('.register-form-client-div').addClass('animated fadeInRight').show();
        });
        $('#investor_toggle_form').click(function () {
            $('.register-form-client-div').hide();
            $('.register-form-investor-div').addClass('animated fadeInLeft').show();
        });
        $('.btn-style-toggle.left-btn').on('click', function () {
            $(this).addClass('active-btn-toggle');
            $('.btn-style-toggle.right-btn').removeClass('active-btn-toggle');
        });
        $('.btn-style-toggle.right-btn').on('click', function () {
            $(this).addClass('active-btn-toggle');
            $('.btn-style-toggle.left-btn').removeClass('active-btn-toggle');
        });
</script>
@endsection

