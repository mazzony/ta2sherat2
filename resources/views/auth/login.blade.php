@extends('layouts.app')
@section('content')
@php
if(session()->has('applocale')){
    if(session()->get('applocale')=='en')
     App::setLocale('en');
    else
    App::setLocale('ar');
}else {
    App::setLocale('ar');
}

@endphp

<section id="Home-Login" class="cd-section">
    <div>
        <div class="container">
            <div class="row">
                <style>
                    .checkbox input[type="checkbox"]:checked + span,
                    .radio input[type="radio"]:checked + span{
                        background-color: #fff!important;
                        color: #33414A !important;
                        /* border-top-right-radius: 30px; */
                        /* border-bottom-left-radius: 30px; */
                        /* box-shadow: 5px 4px 2px #eee; */
                        box-shadow: 0 0 30px 0 rgba(0,0,0,.1);
                        border-bottom: 4px solid #1AD9C7;
                    }
                    *[dir="rtl"] .checkbox input[type="checkbox"]:checked + span.right,
                    *[dir="rtl"] .radio input[type="radio"]:checked + span.right{
                        border-top-left-radius: 50px !important;
                    }
                    *[dir="rtl"] .checkbox input[type="checkbox"]:checked + span.left,
                    *[dir="rtl"] .radio input[type="radio"]:checked + span.left{
                        border-top-right-radius: 50px !important;
                    }
                    *[dir="ltr"] .checkbox input[type="checkbox"]:checked + span.right,
                    *[dir="ltr"] .radio input[type="radio"]:checked + span.right {
                        border-top-right-radius: 50px !important;
                    }
                    *[dir="ltr"] .checkbox input[type="checkbox"]:checked + span.left,
                    *[dir="ltr"] .radio input[type="radio"]:checked + span.left{
                        border-top-left-radius: 50px !important;
                    }
                </style>
                <div class="col-xl-5 mx-auto animated fadeInUp">
                    <div class="form-login card shadow rounded">
                        <form class="px-4 pb-4" id="login-form-admin" method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="text-center mb-5 mt-4">
                                <div class="circle-shape-icon position-relative shadow mx-auto">
                                    <img class="center-box" src="assets/img/svg/user.svg" alt="logo-user">
                                </div>
                            </div>
                            <div>
                                <label for="admin_name" class="label-form d-block">{{ __('lang.E-Mail Address') }}</label>
                                <div class="form-group text-right">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <div class="icon-form-group">
                                        <i class="fa fa-user icon-input-form"></i>

                                    </div>
                                </div>
                            </div>
                            <div>
                                <label for="admin_password" class="label-form d-block">{{ __('lang.Password') }}</label>
                                <div class="form-group text-right">
                                    <div class="div-show-hidden-char-password">
                                        <button class="show_password_btn" type="button"><i class="fa fa-eye pl-3 pr-3"></i></button>
                                    </div>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                    @if (Route::has('password.request'))
                            <a class="btn-nav nav-link p-0"  id="flip-forget-password"  href="{{ route('password.request') }}" style="color: #1AD9C7;">
                                {{ trans('lang.reset') }}
                            </a>

                                @endif
                                    <div class="icon-form-group">
                                        <i class="fa fa-lock icon-input-form"></i>
                                    </div>
                                </div>
                            </div>
                            <div>
                                {{-- <div class="form-group">

                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="label-form d-block" for="remember">
                                                {{ __('lang.Remember Me') }}
                                            </label>

                                </div> --}}
                            </div>
                            <div class="text-center mt-5 mb-5">
                                <button type="submit" class="ml-3 btn btn-global btn-border-rounded-green btn-white-color bg-padding-btn bg-btn-green shadow">{{ __('lang.Login') }}</button>
                            </div>

                        </form>
                         {{-- <a class="btn-nav nav-link p-0"  id="flip-forget-password"  href="/nsso.html" style="color: #1AD9C7;">{{ trans('lang.sso') }}</a> --}}

                        <div class="text-right border-top bg-light p-3">
                      
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


</section>

@endsection

