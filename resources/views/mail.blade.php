@php
    $url = url('/');
@endphp


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>MIM</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<style type="text/css">
		a[x-apple-data-detectors] {color: inherit !important;}
	</style>

</head>
<body style="margin: 0; padding: 0;">
  <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td style="padding: 20px 0 30px 0;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; ">
          <tr>
            <td align="center" bgcolor="#1AD9C7" style="padding-bottom: 50px;">
              <img src="http://192.254.255.70/ticket/assets/img/logo_email.png" alt="Creating Email Magic." width="200" style="  display: block;  " />
            </td>
          </tr>
          <tr>
            <td bgcolor="#f9f9f9" style="padding: 40px 30px 40px 30px;">
              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;" >
                <tr >
                  <td style="color: #153643; font-family: Arial, sans-serif; ">
					<h1 style="font-size: 24px; text-align:center; margin-bottom:20px;">

						<span>خدمة استثناء خروج وعودة</span>
					</h1>
                  </td>
                </tr>
                <tr>
                  <td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
                    @if($status=='new')
                    <p>  عزيزي الشريك نفيدكم بأنه
                        تم تقديم طلبكم لخدمة استثناء خروج وعودة</p>
                 <p>و رقم طلبكم هو {{$id}}</p>
                    <p>وسوف يتم الرد عليكم
                        بأسرع وقت ممكن بإذن الله.</p>

                @endif

                @if($status=='processed')
                <p>عزيزي الشريك نفيدكم بإنه تم الرد على طلبكم رقم {{$id}}</p>

                <p>يمكنكم الاطلاع على تفاصيل الطلب من خلال الضغط هنا </p>


                <a href="{{$url}}/user/request/view_request/{{$id}}."> {{$url}}/user/request/view_request/{{$id}}</a>

                @endif

                @if($status=='refused')
                <p>عزيزي الشريك نفيدكم بإنه تم الرد على طلبكم رقم {{$id}}</p>

                <p>يمكنكم الاطلاع على تفاصيل الطلب من خلال الضغط هنا </p>

                @endif

                @if($status=='returned')
                <p>عزيزي الشريك نفيدكم بإنه تم الرد على طلبكم رقم {{$id}}</p>

                <p>يمكنكم الاطلاع على تفاصيل الطلب من خلال الضغط هنا </p>

                <a href="{{$url}}/user/request/view_request/{{$id}}."> {{$url}}/user/request/view_request/{{$id}}</a>

                @endif
                  </td>
                </tr>

                <tr>

                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td bgcolor="#162c53" style="padding: 30px 30px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
                <tr>
                  <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
                    <p style="margin: 0;"> ©  حقوق الطبع والنشر</span> 2021 <br/>
                   <a href="https://mim.gov.sa" target="_blank" style="color: #ffffff;">وزارة الصناعة والثروة المعدنية</a></p>
				  </td>
				  <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                  <td align="right" style="margin-left:3px; margin-right:3px;">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                      <tr>
                        <td>
                          <a target="_blank" href="https://www.linkedin.com/company/mimgov">
                            <img alt="twitter" width="30" src="http://192.254.255.70/ticket/assets/img/linkedin.png" style="
                                background-color: #ffffff;
                                border-radius: 10px;
                                padding: 4px;
                            ">
                          </a>
                        </td>
                        <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                        <td>
                          <a target="_blank"  href="https://twitter.com/mimgov">
                            <img alt="twitter" width="30" src="http://192.254.255.70/ticket/assets/img/twitter.png" style="
                                background-color: #ffffff;
                                border-radius: 10px;
                                padding: 4px;
                            ">
                          </a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
