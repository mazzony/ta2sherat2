<!-- File: ./resources/views/layouts/master.blade.php -->
<!DOCTYPE html>

@if(session()->has('applocale'))
    @php
    App::setLocale(session()->get('applocale'));
    @endphp
@else
    @php
    App::setLocale('ar');
    session()->put('applocale','ar');
    @endphp
@endif

@if(session()->has('applocale'))
    @if(session()->get('applocale')=='ar')
        @php
            $logo_product = 'assets/img/logo_product_ar.png';
            App::setLocale('ar');
        @endphp
    <html lang="ar" dir="rtl">
    @else
        @php
            $logo_product = 'assets/img/logo_product_en.png';
            App::setLocale('en');
        @endphp
    <html lang="en" dir="ltr">
    @endif
@else
    <html lang="ar" dir="rtl">
        @php
        $logo_product = 'assets/img/logo_product_ar.png';
        App::setLocale('ar');
        @endphp
@endif

      <head>


        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>خدمة استثناء خروج وعودة</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="img/favicon.png" rel="icon">
        <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">



        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.2/vue.js" defer></script>
        <script src="https://kit.fontawesome.com/21e3fbc170.js" crossorigin="anonymous"></script>


        <!-- Vendor CSS Files -->
        <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="plugins/icofont/icofont.min.css" rel="stylesheet">
        <link href="plugins/boxicons/css/boxicons.min.css" rel="stylesheet">
        <link href="plugins/venobox/venobox.css" rel="stylesheet">
        <link href="plugins/remixicon/remixicon.css" rel="stylesheet">
        <link href="plugins/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="plugins/aos/aos.css" rel="stylesheet">
        @if(session()->has('applocale'))
            @if(session()->get('applocale')=='ar')
                <link href="assets/css/style.css" rel="stylesheet">
                <link href="assets/css/responsive.css" rel="stylesheet">
            <script>
                localStorage.setItem('locale','ar');
            </script>
            @else
                <link href="assets/css/style.css" rel="stylesheet">
                                <link href="assets/css/responsive.css" rel="stylesheet">
            <script>
                localStorage.setItem('locale','en');
            </script>
            @endif
        @else
                <link href="assets/css/style.css" rel="stylesheet">
                <link href="assets/css/responsive.css" rel="stylesheet">
            <script>
                localStorage.setItem('locale','ar');
            </script>
        @endif


      </head>
      <body id="body-home-login">

        <div id="preloader-container">
            <div id="preloader-wrap">
                <div class="cssload-thecube">
                    <div class="cssload-cube cssload-c1"></div>
                    <div class="cssload-cube cssload-c2"></div>
                    <div class="cssload-cube cssload-c4"></div>
                    <div class="cssload-cube cssload-c3"></div>
                </div>
            </div>
        </div>

        @include('login_header')


        <div id="app">
            @yield('content')
        </div>



          @include('inner_footer')

          <style>
            .about_btn {
                background: rgba(0,0,0,0.5) !important;
                border-color: #1AD9C7!important;
                color: #fff !important;
            }
            .about_btn:hover {
                background: #1AD9C7!important;
            }
            .link_terms {
                font-size: 1.5rem;
                position: absolute;
                bottom: .3rem;
                right: .3rem;
            }
            *[dir="ltr"] .link_terms {
                right: auto;
                left: .3rem;
            }
            .link_terms:hover {
                background-color: #1AD9C7;
                color: #fff;
            }
            .link_hover_sidf:hover {
                color: #1AD9C7!important;
            }
            .list_footer {
                list-style: none;
            }
            .list_footer li:not(:first-child) {
                margin-top: .8rem;
            }
            .list_footer li > a {
                display: inline-block;
            }
            .list_footer li > a:hover {
                color: #fff !important;
            }
            .list_footer li:before {
                content: "\f10c";
                font-family: "FontAwesome";
                margin-left: 1rem;
                font-size: 1.4rem;
                color: #1AD9C7;
            }
            *[dir="ltr"] .list_footer li:before {
                margin-left: 0;
                margin-right: 1rem;
            }
        </style>
        @include('footer_links')
        <script src="assets/js/custom.js"></script>
        <script>
            $(window).on('load',function () {
                setTimeout(function () {
                    $('.header_section_title').addClass('animated fadeInDownBig');
                    $('.card_home_cover_dark').addClass('animated fadeInUpBig');
                }, 500);
            });

            $(function () {
                /* show_conditions link */
                var dir = true;
                if($('html').attr('dir') === 'ltr'){
                    dir = false;
                }
                $(document).on('click','.show_conditions',function () {
                    $.confirm({
                        columnClass: 'col-xl-8 col-lg-12',
                        closeIcon: true,
                        type: 'dark',
                        animation: 'scale',
                        closeAnimation: 'bottom',
                        rtl: dir,
                        buttons: {
                            close:{
                                text: '{{__('about_nesba_green')}}',
                                btnClass: 'btn-red',
                                action: function(){

                                }
                            }
                        },
                        // content: function () {
                        //     var self = this;
                        //     return $.ajax({
                        //         url: '<?='base/get_conditions';?>',
                        //         dataType: 'JSON',
                        //         method: 'POST'
                        //     }).done(function (response) {
                        //         self.setContent(response.html);
                        //         self.setTitle(response.title);
                        //     }).fail(function(){
                        //         self.setContent('Something went wrong.');
                        //     });
                        // }
                    });
                });
            });
        </script>

          <!-- Vendor JS Files -->
          <script src="plugins/jquery/jquery.min.js"></script>
          <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
          <script src="plugins/jquery.easing/jquery.easing.min.js"></script>
          <script src="plugins/php-email-form/validate.js"></script>
          <script src="plugins/waypoints/jquery.waypoints.min.js"></script>
          <script src="plugins/counterup/counterup.min.js"></script>
          <script src="plugins/venobox/venobox.min.js"></script>
          <script src="plugins/owl.carousel/owl.carousel.min.js"></script>
          <script src="plugins/isotope-layout/isotope.pkgd.min.js"></script>
          <script src="plugins/aos/aos.js"></script>

          <!-- Template Main JS File -->
          <script src="js/main.js"></script>
          <script>
                function lang_switch() {
                    var switcher = $('#switcher').text();
                    var lang ='';

                    if(switcher=='عربي'){
                        lang='ar';
                    }else{
                        lang='en';
                    }
                    $.ajax({url: "api/set_locale/"+lang,
                    success: function(result){
                        location.reload(true);
                        localStorage.setItem('locale',lang);
                        }});


                };

          </script>
      </body>
    </html>
