<!-- File: ./resources/views/layouts/master.blade.php -->
<!DOCTYPE html>

@if(session()->has('applocale'))
    @php
    App::setLocale(session()->get('applocale'));
    @endphp
@else
    @php
    App::setLocale('ar');
    session()->put('applocale','ar');
    @endphp
@endif

@if(session()->has('applocale'))
    @if(session()->get('applocale')=='ar')
        @php
            $logo_product = 'assets/img/logo_product_ar.png';
            App::setLocale('ar');
        @endphp
    <html lang="ar" dir="rtl">
    @else
        @php
            $logo_product = 'assets/img/logo_product_en.png';
            App::setLocale('en');
        @endphp
    <html lang="en" dir="ltr">
    @endif
@else
    <html lang="ar" dir="rtl">
        @php
        $logo_product = 'assets/img/logo_product_ar.png';
        App::setLocale('ar');
        @endphp
@endif

      <head>


        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <link rel="icon" href="/ta2shera/public/assets/img/favicon.ico">

        <title>خدمة استثناء خروج وعودة</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->


        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">



        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.2/vue.js" defer></script>
        <script src="https://kit.fontawesome.com/21e3fbc170.js" crossorigin="anonymous"></script>


        <!-- Vendor CSS Files -->
        <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="plugins/icofont/icofont.min.css" rel="stylesheet">
        <link href="plugins/boxicons/css/boxicons.min.css" rel="stylesheet">
        <link href="plugins/venobox/venobox.css" rel="stylesheet">
        <link href="plugins/remixicon/remixicon.css" rel="stylesheet">
        <link href="plugins/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="plugins/aos/aos.css" rel="stylesheet">
        <link rel="shortcut icon" href="/ta2shera/public/assets/img/favicon.ico">
        <link href="/ta2shera/public/assets/css/wizard/jquery.steps.css" rel="stylesheet" type="text/css" />
        <link href="/ta2shera/public/assets/css/dialog/jquery-confirm.min.css" rel="stylesheet" type="text/css" />
        <link href="/ta2shera/public/assets/css/emoji/emojionearea.min.css" rel="stylesheet" type="text/css" />
        <link href="/ta2shera/public/assets/css/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />
        @if(session()->has('applocale'))
            @if(session()->get('applocale')=='ar')
                <link href="/ta2shera/public/assets/css/style.css" rel="stylesheet">

                <link href="/ta2shera/public/assets/css/responsive.css" rel="stylesheet">
            <script>
                localStorage.setItem('locale','ar');
            </script>
            @else
                <link href="/ta2shera/public/assets/css/style.css" rel="stylesheet">
                 <link href="/ta2shera/public/assets/css/responsive.css" rel="stylesheet">
            <script>
                localStorage.setItem('locale','en');
            </script>
            @endif
        @else
                <link href="/ta2shera/public/assets/css/style.css" rel="stylesheet">
                <link href="/ta2shera/public/assets/css/responsive.css" rel="stylesheet">
            <script>
                localStorage.setItem('locale','ar');
            </script>
        @endif


      </head>
      <body>

        <div id="preloader-container">
            <div id="preloader-wrap">
                <div class="cssload-thecube">
                    <div class="cssload-cube cssload-c1"></div>
                    <div class="cssload-cube cssload-c2"></div>
                    <div class="cssload-cube cssload-c4"></div>
                    <div class="cssload-cube cssload-c3"></div>
                </div>
            </div>
        </div>

        @include('header_new')


        <div id="app">
            @yield('content')
        </div>



          @include('footer')

          <style>
            .about_btn {
                background: rgba(0,0,0,0.5) !important;
                border-color: #1AD9C7!important;
                color: #fff !important;
            }
            .about_btn:hover {
                background: #1AD9C7!important;
            }
            .link_terms {
                font-size: 1.5rem;
                position: absolute;
                bottom: .3rem;
                right: .3rem;
            }
            *[dir="ltr"] .link_terms {
                right: auto;
                left: .3rem;
            }
            .link_terms:hover {
                background-color: #1AD9C7;
                color: #fff;
            }
            .link_hover_sidf:hover {
                color: #1AD9C7!important;
            }
            .list_footer {
                list-style: none;
            }
            .list_footer li:not(:first-child) {
                margin-top: .8rem;
            }
            .list_footer li > a {
                display: inline-block;
            }
            .list_footer li > a:hover {
                color: #fff !important;
            }
            .list_footer li:before {
                content: "\f10c";
                font-family: "FontAwesome";
                margin-left: 1rem;
                font-size: 1.4rem;
                color: #1AD9C7;
            }
            *[dir="ltr"] .list_footer li:before {
                margin-left: 0;
                margin-right: 1rem;
            }
        </style>
        @include('footer_links')
        <script src="/ta2shera/public/assets/js/custom.js"></script>


          <!-- Vendor JS Files -->
          <script src="plugins/jquery/jquery.min.js"></script>
          <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
          <script src="plugins/jquery.easing/jquery.easing.min.js"></script>
          <script src="plugins/php-email-form/validate.js"></script>
          <script src="plugins/waypoints/jquery.waypoints.min.js"></script>
          <script src="plugins/counterup/counterup.min.js"></script>
          <script src="plugins/venobox/venobox.min.js"></script>
          <script src="plugins/owl.carousel/owl.carousel.min.js"></script>
          <script src="plugins/isotope-layout/isotope.pkgd.min.js"></script>
          <script src="plugins/aos/aos.js"></script>
          <script src="/ta2shera/public/assets/js/wow/wow.min.js"></script>
          <script src="/ta2shera/public/assets/js/jquery.min.js"></script>
          <script src="/ta2shera/public/assets/js/jquery-ui/jquery-ui.js"></script>
          <!-- popper -->
          <script src="/ta2shera/public/assets/js/popper/popper.min.js"></script>
          <script src="/ta2shera/public/assets/js/popper/moment.min.js"></script>
          <!-- bootstrap -->
          <script src="/ta2shera/public/assets/js/bootstrap/bootstrap.min.js"></script>
          <!-- bootstrap-select -->
          <script src="/ta2shera/public/assets/js/bootstrap-select/bootstrap-select.min.js"></script>
          <!-- bootstrap-date -->
          <script src="/ta2shera/public/assets/js/bootstrap-date/bootstrap-datetimepicker.min.js"></script>
          <!-- OWL -->
          <script src="/ta2shera/public/assets/js/owl-carousel/owl.carousel.min.js"></script>
          <!-- easing -->
          <script src="/ta2shera/public/assets/js/easing/jquery.easing.1.3.js"></script>
          <!-- equal height -->
<script src="/ta2shera/public/assets/js/matchHeight/jquery.matchHeight-min.js"></script>
<!-- niceScroll -->
<script src="/ta2shera/public/assets/js/niceScroll/jquery.nicescroll.min.js"></script>
<!-- parsley -->
<script src="/ta2shera/public/assets/js/parsley/parsley.min.js"></script>
<!-- Toast -->
<script src="/ta2shera/public/assets/js/toast/toast.min.js"></script>
<!-- Typed -->
<script src="/ta2shera/public/assets/js/typed/typed.min.js"></script>
<!-- select-lib -->
<script src="/ta2shera/public/assets/js/select-lib/jquery.selectric.min.js"></script>
<!-- wizard -->
<script src="/ta2shera/public/assets/js/wizard/jquery.steps.min.js"></script>
<!-- repeater -->
<script src="/ta2shera/public/assets/js/repeater/jquery.repeater.min.js"></script>
<!-- dialog -->
<script src="/ta2shera/public/assets/js/dialog/jquery-confirm.min.js"></script>
<!-- emoji -->
<script src="/ta2shera/public/assets/js/emoji/emojionearea.min.js"></script>
<!-- popup image -->
<script src="/ta2shera/public/assets/js/magnific-popup/jquery.magnific-popup.min.js"></script><script src="/ta2shera/public/assets/js/custom.js"></script>

          <!-- Template Main JS File -->
          <script src="js/main.js"></script>
          <script>
            $(window).on('load',function () {
                setTimeout(function () {
                    $('.header_section_title').addClass('animated fadeInDownBig');
                    $('.card_home_cover_dark').addClass('animated fadeInUpBig');
                }, 500);
            });

            $(function () {
                /* show_conditions link */
                var dir = true;
                if($('html').attr('dir') === 'ltr'){
                    dir = false;
                }
                $(document).on('click','.show_conditions',function () {
                    $.confirm({
                        columnClass: 'col-xl-8 col-lg-12',
                        closeIcon: true,
                        type: 'dark',
                        animation: 'scale',
                        closeAnimation: 'bottom',
                        rtl: dir,
                        buttons: {
                            close:{
                                text: '{{__('lang.close')}}',
                                btnClass: 'btn-red',
                                action: function(){

                                }
                            }
                        },
                        //  content: function () {
                        //     var self = this;
                        //     self.setContent("<div><h1>ssssss</h1></div>");
                        //     self.setTitle('<b class="text-right border-bottom pb-2 border-secondary">الشروط والأحكام</b>');

                        // }
                        content: function () {
                            var self = this;
                            return $.ajax({
                                url: 'api/get_conditions',
                                dataType: 'JSON',
                                method: 'POST'
                            }).done(function (response) {
                                self.setContent(response.html);
                                self.setTitle(response.title);
                            }).fail(function(){
                                self.setContent('الشروط والاحكام');
                                self.setTitle('error');
                            });
                        }
                    });
                });
            });
        </script>
          <script>
                function lang_switch() {

                    var switcher = $('#switcher').text();
                    var lang ='';

                    if(switcher=='ع'){
                        lang='ar';

                    }else{
                        lang='en';
                    }
                    $.ajax({url: "api/set_locale/"+lang,
                    success: function(result){
                        location.reload(true);
                        localStorage.setItem('locale',lang);

                        }});


                };

          </script>
      </body>
    </html>
