@extends('layouts.app')

@section('content')
@php
if(session()->has('applocale')){
    if(session()->get('applocale')=='en')
     App::setLocale('en');
    else
    App::setLocale('ar');
}else {
    App::setLocale('ar');
}

@endphp
<section id="Home">
    <div class="cover-home">
        <div class="inner-div">
            <div class="inside-inner-div">
                <h2 style="font-weight: 700; font-size: 2.5rem; margin-top: 5rem; color: #fff;" class="header_section_title text-center">{{__('lang.intro')}}</h2>
                <div style="margin-top: 4rem;" class="container">
                    <div class="row">
                        <div class="col-lg-4 text-center">
                            <div class="card_home_cover_dark my-2">
                                <div class="circle-shape-image mx-auto position-relative">
                                    <img class="center-box" style="height: 5rem;" src="assets/img/nesba_icon_2.png"/>
                                </div>
                                <h2 style="font-weight: 600; font-size: 2.2rem; color: #fff;" class="header_title_icon mt-4 border-secondary pb-3 text-center">{{__('lang.h1')}}</h2>
                                <p style="font-size: 1.8rem;" class="p-2 header_desc_icon mt-4 text-white text-center">
                                    {{__('lang.d1')}}
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-4 text-center">
                            <div class="card_home_cover_dark my-2">
                                <div class="circle-shape-image mx-auto position-relative">
                                    <img class="center-box" style="height: 5rem;" src="assets/img/nesba_icon_1.png"/>
                                </div>
                                <h2 style="font-weight: 600; font-size: 2.2rem; color: #fff;" class="header_title_icon mt-4 border-secondary pb-3 text-center">{{__('lang.h2')}}</h2>
                                <p style="font-size: 1.8rem;" class="p-2 header_desc_icon mt-4 text-white text-center">
                                    {{__('lang.d2')}}
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-4 text-center">
                            <div class="card_home_cover_dark my-2">
                                <div class="circle-shape-image mx-auto position-relative">
                                    <img class="center-box" style="height: 5rem;" src="assets/img/nesba_icon_3.png"/>
                                </div>
                                <h2 style="font-weight: 600; font-size: 2.2rem; color: #fff;" class="header_title_icon mt-4 border-secondary pb-3 text-center">{{__('lang.h3')}}</h2>
                                <p style="font-size: 1.8rem;" class="p-2 header_desc_icon mt-4 text-white text-center">
                                    {{__('lang.d3')}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btns-home-section text-center">
                    <a href="register" class="ml-2 my-2 btn btn-global btn-border-rounded-green btn-white-color bg-padding-btn bg-btn-green shadow">{{__('lang.Register')}}</a>
                    <a href="#AboutProduct" class="about_btn ml-2 my-2 smooth-scroll btn btn-global btn-border-rounded-white btn-green-color bg-padding-btn bg-btn-white shadow">{{__('lang.about_product')}}</a>
                </div>
            </div>
            <a href="javascript:void(0)" class="show_conditions link_terms border d-inline-block text-white nav-link">{{__('lang.terms')}}</a>
        </div>
    </div>
</section>
<section id="AboutProduct" class="py-5 bg-white">
    <div class="wrapper-section">
        <div class="container py-5">
            <div class="row">
                <div class="col-xl-12 text-right" >
                    <div class="title-about-section">
                        <h1 class="text-dark border-bottom"> {{__('lang.about_nesba_green')}}</h1>
                        <p class="my-5 text-justify">
                            {{__('lang.about_details')}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

