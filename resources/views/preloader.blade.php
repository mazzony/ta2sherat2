<div id="preloader" class="preloader-container" >
    <div class="preloader-wrap text-center">
		<div class="position-relative">
			<div class="spinner-border text-secondary" style="width: 5rem; height: 5rem;" role="status">
				<span class="sr-only">Loading...</span>
			</div>
			<img class="center_box" height="50" src="https://mim.gov.sa/assets/img/loader.png" alt="loader">
		</div>
		<small class="d-block text-secondary my-2 text-center">الرجاء الإنتظار ...</small>
    </div>
</div>
