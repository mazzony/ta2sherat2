<footer style="margin-top: 6rem;">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 border-top border-secondary pr-0 pl-0">
                <div class="pt-2">
                    <label style="font-size: 1.8rem" class="pull-right d-inline-block text-muted"><span class="mx-1">{{__('lang.footer_copy_right')}}</span> <?=date('Y');?> &copy;</label>
                    <div class="d-inline-block pull-left">
                        @if(session()->has('applocale'))

                            @if(session()->get('applocale')=='ar')
                                <a href="#" class="shadow-sm btn toggle_language_site_light" onclick="lang_switch()">
                                    <i class="fa fa-globe pr-2 pl-2"></i><span class="btn_text">EN</span>
                                </a>
                            @else
                                <a href="#" class="shadow-sm btn toggle_language_site_light" onclick="lang_switch()">
                                    <i class="fa fa-globe pr-2 pl-2"></i><span class="btn_text">عربي</span>
                                </a>
                            @endif
                        @else
                            <a href="#" class="shadow-sm btn toggle_language_site_light" onclick="lang_switch()">
                                <i class="fa fa-globe pr-2 pl-2"></i><span class="btn_text">EN</span>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

