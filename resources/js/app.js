require('./bootstrap');
window.default_locale = "ar";
window.fallback_locale = "ar";
import 'vue-search-select/dist/VueSearchSelect.css'
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueLang from '@eli5/vue-lang-js'
import translations from './vue-translations.js';
import moment from 'moment'
import VueSimpleAlert from "vue-simple-alert";


import SideNavv from "./components/sidebar/ResponsiveSideNav.vue";
import Homepage from './components/Homepage'
import PlaceRequest from './components/orders/PlaceRequest'
import MyRequests from './components/requests/MyRequests'
import UserViewRequest from './components/requests/ViewRequest'
import UserEditRequest from './components/requests/EditRequest'

import Inactive from './components/users/Read'


import Admins from './components/admins/Read'
import AdminAdd from './components/admins/Create'
import AdminEdit from './components/admins/Update'


import Evaluators from './components/evaluators/Read'
import Certifiers from './components/certifier/Read'
import AddEvaluator from './components/evaluators/Create'
import AddCertifier from './components/certifier/Create'
import EvaluatorEdit from './components/evaluators/Update'
import EvaluatorViewRequests from './components/evaluators/ViewRequests'
import EvaluatorViewRequest from './components/evaluators/ViewRequest'

import CertifierViewRequests from './components/certifier/ViewRequests'
import CertifierViewRequest from './components/certifier/ViewRequest'




import TermsAndConditions from './components/requests/TermsAndConditions'
import AddRequest from './components/requests/PlaceRequest'

import ViewRequests from './components/traders/ViewRequests'
import AdminViewRequests from './components/traders/AdminViewRequests'
import ViewRequest from './components/traders/ViewRequest'
import AdminViewRequest from './components/traders/AdminViewRequest'

import pdf from './components/orders/pdf'

import Profile from './components/users/Profile'


import DatatableFilter from './components/users/datatable/UserDatatableFilter'
import VueSidebarMenu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'
import SideBar from './components/sidebar/Sidebar.vue'
import LineChart from './components/charts/LineChart.vue'
import Line from "vue-chartjs";
import DataTable from 'laravel-vue-datatable';
import csbutton from './components/users/datatable/ChangeStatusButton.vue';
import JwPagination from 'jw-vue-pagination';
import jsPDF from 'jsPDF'
import VueLoading from 'vue-loading-overlay';

const default_locale = window.default_language;
const fallback_locale = window.fallback_locale;
Vue.use(VueRouter)
Vue.use(VueLang, {
    messages: translations, // Provide locale file
     locale: default_locale, // Set locale
    fallback: fallback_locale // Set fallback lacale
  });
Vue.use(VueSidebarMenu);

Vue.use(VueSimpleAlert);
Vue.use(VueLoading);
Vue.use(DataTable);

Vue.use(jsPDF);
//let token = document.head.querySelector('meta[name="csrf-token"]').content;
//Vue.http.headers.common["X-CSRF-TOKEN"] = token;
Vue.component('SideBar1', SideBar);
Vue.component('LineChart',Line);
Vue.component('data-table-filters',DatatableFilter);
Vue.component('CSButton',csbutton);
Vue.component('SideNav',SideNavv);
Vue.component('jw-pagination', JwPagination);
Vue.component('Loading', VueLoading);

Vue.prototype.moment = moment

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/ta2shera/public/user/profile',
            name: 'Profile',
            component: Profile,
            props: true
        },
        {
            path: '/ta2shera/public/admin/chart',
            name: 'chart',
            component: LineChart,
            props: true
        },
        {
            path: '/ta2shera/public/admin/dashboard',
            name: 'chart',
            component: LineChart,
            props: true
        },
        {
            path: '/ta2shera/public/user/dashboard',
            name: 'userChart',
            component: LineChart,
            props: true
        },

        {
            path: '/ta2shera/public/admin/users',
            name: 'inactive',
            component: Inactive,
            props: true
        },
        {
            path: '/ta2shera/public/admin/admins/list',
            name: 'adminList',
            component: Admins,
            props: true
        },{
            path: '/ta2shera/public/admin/admins/add',
            name: 'adminAdd',
            component: AdminAdd,
            props: true
        },{
            path: '/ta2shera/public/admin/admins/edit/:id',
            name: 'adminEdit',
            component: AdminEdit,
            props: true
        },
        {
            path: '/ta2shera/public/admin/evaluators/list',
            name: 'evluatorList',
            component: Evaluators,
            props: true
        },
        {
            path: '/ta2shera/public/admin/certifiers/list',
            name: 'superevluatorList',
            component: Certifiers,
            props: true
        },
        {
            path: '/ta2shera/public/admin/evaluators/add',
            name: 'evaluatorAdd',
            component: AddEvaluator,
            props: true
        },
        {
            path: '/ta2shera/public/admin/certifiers/add',
            name: 'superevaluatorAdd',
            component: AddCertifier,
            props: true
        },

        {
            path: '/ta2shera/public/admin/evaluators/edit/:id',
            name: 'evaluatorEdit',
            component: EvaluatorEdit,
            props: true
        },
        {
            path: '/ta2shera/public/user/evaluators/requests',
            name: 'evaluatorRequests',
            component: EvaluatorViewRequests,
            props: true
        },
        {
            path: '/ta2shera/public/user/certifiers/requests',
            name: 'certifierRequests',
            component: CertifierViewRequests,
            props: true
        },
        {
            path: '/ta2shera/public/user/evaluator/view_request/:id',
            name: 'evaluatorRequest',
            component: EvaluatorViewRequest,
            props: true
        },
        {
            path: '/ta2shera/public/user/certifier/view_request/:id',
            name: 'certifierRequest',
            component: CertifierViewRequest,
            props: true
        },



       {
            path: '/ta2shera/public/user/order/place_request',
            name: 'PlaceRequest',
            component: PlaceRequest,
            props: true
        },

       {
        path: '/ta2shera/public/user/request/TermsAndConditions',
        name: 'TermsAndConditions',
        component: TermsAndConditions,
        props: true
    },

        {
            path: '/ta2shera/public/user/request/place_request',
            name: 'AddRequest',
            component: AddRequest,
            props: true
        },

        {
            path: '/ta2shera/public/user/request/my_requests',
            name: 'MyRequests',
            component: MyRequests,
            props: true
        },
        {
            path: '/ta2shera/public/user/request/edit_request/:id',
            name: 'UserEditRequest',
            component: UserEditRequest,
            props: true
        },

        {
            path: '/ta2shera/public/user/request/view_request/:id',
            name: 'UserViewRequest',
            component: UserViewRequest,
            props: true
        },
        {
            path: '/ta2shera/public/user/traders/view_requests/:id',
            name: 'ViewRequests',
            component: ViewRequests,
            props: true
        },
        {
            path: '/ta2shera/public/user/traders/view_request/:id',
            name: 'ViewRequest',
            component: ViewRequest,
            props: true
        },
        {
            path: '/ta2shera/public/admin/traders/view_request/:id',
            name: 'AdminViewRequest',
            component: AdminViewRequest,
            props: true
        },

        {
            path: '/ta2shera/public/admin/traders/view_requests/:id',
            name: 'AdminViewRequests',
            component: AdminViewRequests,
            props: true
        },

        {
            path: '/ta2shera/public/admin/pdf',
            name: 'pdf',
            component: pdf,
            props: true
        },



    ],
});

const app = new Vue({
    el: '#app',
    router,
    components: { Homepage  },
});


