<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('s_id')->nullable();
            $table->string('cr');
            $table->string('ir');
            $table->integer('city')->unsigned();
            $table->string('request_id')->nullable();
            $table->string('factory_name');
            $table->string('factory_symbol');
            $table->string('firstName')->nullable();
            $table->string('middleName')->nullable();
            $table->string('lastName')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('accept_number')->nullable();
            $table->date('accept_number_date')->nullable();
            $table->longText('details');
            $table->string('status');
            $table->integer('from')->unsigned();
            $table->string('response');
            $table->integer('responser')->unsigned();
            $table->string('response_comments');
            $table->longText('attachments');
            $table->integer('certifier')->unsigned();
            $table->string('certifier_comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
