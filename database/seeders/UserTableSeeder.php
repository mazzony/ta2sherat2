<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;


class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->firstName = 'Mazen';
        $user->middleName = 'Yehia';
        $user->lastName = 'Omar';
        $user->email = 'mazzony13@gmaill.com';
        $user->password = bcrypt('131313');
        $user->type = "system_user";
        $user->status ='active';
        $user->save();
        $user->roles()->attach(Role::where('name', 'user')->first());

        $admin = new User;
        $admin->firstName = 'Mazen';
        $admin->middleName = 'Yehia';
        $admin->lastName = 'Omar';
        $admin->email = 'mazzony13@outlook.sa';
        $admin->password = bcrypt('131313');
        $admin->type = "system_user";
        $admin->status ='active';
        $admin->save();
        $admin->roles()->attach(Role::where('name', 'admin')->first());
    }
}
