<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Sector;
class SectorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $post = new Sector;
            $post->parent = 0;
            $post->name = "Factories";
            $post->description = "Factories Sector is here ";
            $post->region = "Factories Region";
            $post->save();

            $post = new Sector;
            $post->parent = 0;
            $post->name = "Quarries";
            $post->description = "Quarries Sector is here ";
            $post->region = "Quarries Region";
            $post->save();
    }
}
