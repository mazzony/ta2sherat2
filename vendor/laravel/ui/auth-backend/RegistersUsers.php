<?php

namespace Illuminate\Foundation\Auth;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Support\Facades\Http;

trait RegistersUsers
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\View\View
     */
    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();


        if(isset($request->cr)){
            if($request->cr !="131313"){

                $res = Http::withHeaders([
                    'X-API-KEY' => 'MobileAPP',
                    'web-token' => 'bw&dB]F]?H_BS]g_8;2z!f`^4yU^tpfx',
                    'platform'  => 'web'
                ])->get('https://mim.gov.sa/api/v1/cr/'.$request->cr.'/details');

                $string = $res->getBody()->getContents();
                $string = str_replace('\n', '', $string);
                $string = rtrim($string, ',');
                $string = "[" . trim($string) . "]";
                $json = json_decode($string, true);

                if($json[0]['result']['data'] == null){


                    Session::flash('cr', __('lang.cr_error'));

                    return $request->wantsJson()
                        ? new JsonResponse([], 201)
                        : redirect('/register');

                }

            }





        }

        event(new Registered($user = $this->create($request->all())));

       // $this->guard()->login($user);

        if ($response = $this->registered($request, $user)) {

            return $response;

        }


        Session::flash('message', 'Please wait for your acount activation');

        return $request->wantsJson()
                    ? new JsonResponse([], 201)
                    : redirect('/login');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }
}
