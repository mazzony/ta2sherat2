-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2021 at 03:05 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tamken`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(13, '2014_10_12_000000_create_users_table', 1),
(14, '2014_10_12_100000_create_password_resets_table', 1),
(15, '2019_08_19_000000_create_failed_jobs_table', 1),
(16, '2021_06_07_095152_create_roles_table', 1),
(17, '2021_06_07_095818_create_role_user_table', 1),
(19, '2021_06_16_095959_create_orders_table', 2),
(20, '2021_06_07_104224_create_sectors_table', 3),
(22, '2021_06_16_185708_create_requests_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sector_id` int(10) UNSIGNED NOT NULL,
  `trader_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `title`, `status`, `details`, `sector_id`, `trader_id`, `created_at`, `updated_at`) VALUES
(1, 'Plastic Bags', 'pending', 'I need about  3 Tons of Plastic bags', 5, 29, '2021-06-16 12:38:42', '2021-06-16 12:38:42'),
(2, 'Plastic Bottels', 'pending', '30Tons', 5, 29, '2021-06-16 21:57:59', '2021-06-16 21:57:59'),
(3, 'Glass', 'pending', '30 Meter of Glass Type A', 8, 35, '2021-06-17 12:20:10', '2021-06-17 12:20:10'),
(4, '40 Ton Plastic', 'pending', '40 Ton Plastic', 5, 35, '2021-06-17 12:20:44', '2021-06-17 12:20:44'),
(5, 'Glass Order', 'pending', 'I need 1000 Ton Of pure glass', 8, 35, '2021-06-21 08:13:25', '2021-06-21 08:13:25'),
(6, '40 طن', 'pending', 'اريد 40 طن زجاج', 8, 29, '2021-06-23 12:00:20', '2021-06-23 12:00:20');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from` int(10) UNSIGNED NOT NULL,
  `to` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `sector_id` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `status`, `details`, `from`, `to`, `order_id`, `sector_id`, `price`, `created_at`, `updated_at`) VALUES
(1, 'pending', 'For 10 Tons', 30, 29, 2, 5, 50000.00, '2021-06-17 10:06:27', '2021-06-17 10:06:27'),
(2, 'pending', '80 for ton', 30, 29, 1, 5, 80000.00, '2021-06-17 10:35:39', '2021-06-17 10:35:39'),
(3, 'pending', '1333 for ton', 31, 29, 2, 5, 20000.00, '2021-06-17 11:12:40', '2021-06-17 11:12:40'),
(4, 'Accepted', 'will offer you meter with 1000 SAR', 33, 35, 3, 8, 30000.00, '2021-06-17 12:38:46', '2021-06-17 12:40:12'),
(5, 'Accepted', 'For Ton', 33, 35, 5, 8, 3000.00, '2021-06-21 08:16:32', '2021-06-21 08:17:43'),
(6, 'Accepted', 'اعرض ب 3500 لكل طن', 33, 29, 6, 8, 3500.00, '2021-06-23 12:01:53', '2021-06-23 12:02:19');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'user', 'A regular user', '2021-06-10 09:13:14', '2021-06-10 09:13:14'),
(2, 'admin', 'An admin user', '2021-06-10 09:13:14', '2021-06-10 09:13:14'),
(3, 'trader', 'Trader', '2021-06-10 09:13:14', '2021-06-10 09:13:14'),
(4, 'factory', 'factory user', '2021-06-10 09:13:14', '2021-06-10 09:13:14'),
(5, 'quarry', 'quarry user', '2021-06-10 09:13:14', '2021-06-10 09:13:14');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 4, 9),
(4, 4, 10),
(5, 5, 12),
(6, 3, 13),
(7, 1, 15),
(8, 1, 16),
(9, 1, 17),
(10, 1, 18),
(11, 1, 19),
(12, 1, 20),
(13, 1, 21),
(14, 1, 22),
(15, 1, 23),
(16, 5, 24),
(17, 1, 24),
(18, 2, 25),
(19, 2, 26),
(20, 4, 27),
(21, 3, 28),
(22, 3, 29),
(23, 4, 30),
(24, 5, 31),
(25, 3, 32),
(26, 4, 33),
(27, 5, 34),
(28, 3, 35),
(29, 4, 36);

-- --------------------------------------------------------

--
-- Table structure for table `sectors`
--

CREATE TABLE `sectors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sectors`
--

INSERT INTO `sectors` (`id`, `name`, `description`, `region`, `featured_image`, `parent`, `created_at`, `updated_at`) VALUES
(5, 'Plastic', 'Plastic Sector', 'KSA', '1623849177_505429_plasticbottlescrop_8719.jpg', 0, '2021-06-16 11:12:57', '2021-06-16 11:12:57'),
(6, 'Metals', 'Metals sector', 'KSA', '1623872077_metal-1.jpg', 0, '2021-06-16 17:34:37', '2021-06-17 12:12:47'),
(8, 'Glass', 'Glass Sector', 'KSA', '1623939251_glass-reality-glass-trade-fair.jpg', 0, '2021-06-17 12:14:11', '2021-06-17 12:14:11'),
(9, 'Oil', 'Oil Sector', 'KSA', '1624270039_oils.jpg', 0, '2021-06-21 08:07:19', '2021-06-21 08:07:19'),
(10, 'Wood', 'Wood Sector', 'KSA', '1624455885_Science_wood_1136989911.jpg', 0, '2021-06-23 11:44:45', '2021-06-23 11:44:45'),
(11, 'بترول', 'قطاع البترول', 'KSA', '1624456688_energy-oil-pumps-2.jpg', 0, '2021-06-23 11:58:08', '2021-06-23 11:58:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sector` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `sector`, `status`, `type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mazen Yehia', 'mazzony13@gmail.com', NULL, '$2y$10$M.4pUT.sbTJBlS0VNI.2bODZftFliIhn5XjeWUtuf9C1a7LIDjFNy', 0, 'active', 'system_user', NULL, '2021-06-10 09:13:14', '2021-06-10 09:13:14'),
(2, 'admin Admin', 'mazzony13@outlook.sa', NULL, '$2y$10$ywzfmq9kRr4.cB4yaGiuOutGD0WKMw2IYx1t3Fk4wHdFGkQPElRru', 0, 'active', 'system_user', NULL, '2021-06-10 09:13:14', '2021-06-10 09:13:14'),
(14, 'mazen', 'factory22@tamken.com', NULL, '$2y$10$12t2.cGWQoUV3mzNN6NsyuGx2ZNHZat4A04NsSAGklQR.m8L4PMg6', 0, 'inactive', '', NULL, '2021-06-13 07:44:11', '2021-06-13 07:44:11'),
(15, 'mazen', 'factory23@tamken.com', NULL, '$2y$10$9FgaBUbhcIaexgJwnYPtLelCU9ljlY.BtPxwvgrehfpcMjlYbgnHW', 0, 'inactive', '', NULL, '2021-06-13 07:44:57', '2021-06-13 07:44:57'),
(16, 'trader', 'trader@ttest.com', NULL, '$2y$10$HCq3barvt0DK.im1q2i4t.3EQJ5tUED0OixAmj4hyG9c/v7ytLcBi', 0, 'inactive', '', NULL, '2021-06-13 07:48:56', '2021-06-13 07:48:56'),
(17, 'Mazen', 'Quarry1@gmail.com', NULL, '$2y$10$X3S0YsZGG8FZnL1/Wz3s3OhBazLWFvrJnIp3FNkFSuGl3n0LHOlwC', 0, 'inactive', '', NULL, '2021-06-13 07:57:06', '2021-06-13 07:57:06'),
(18, 'lllll', 'wsss@jjjj.com', NULL, '$2y$10$IVal7Z5BiWviNVN2QcWPeeFW1tLLdQnMmoXGlPz7WL.9W/zATdQQO', 0, 'inactive', '', NULL, '2021-06-13 07:59:52', '2021-06-13 07:59:52'),
(19, 'Test', 'ty@gmail.com', NULL, '$2y$10$Xf4t3tfj1Iw1oHR8fVU4IOeBeGyQW33INSss4E00535OWVjfLmvaa', 0, 'inactive', '', NULL, '2021-06-13 08:00:45', '2021-06-13 08:00:45'),
(20, 'mmm', 'mmm@hshsg.com', NULL, '$2y$10$3pNSQbw8AsOo3Xrr/wi.leOFu1VbcLhDG3kw3ldpDSXz4VAZr8jre', 0, 'inactive', '', NULL, '2021-06-13 08:02:02', '2021-06-13 08:02:02'),
(21, 'mmm', 'mm@hshsg.com', NULL, '$2y$10$muDn4GHcN869pbm9TsoKYOi8kWprLpfbsxhOK.gFnAWTcukmoFVhy', 0, 'inactive', '', NULL, '2021-06-13 08:07:00', '2021-06-13 08:07:00'),
(22, 'mmpp', 'ppp@jki.com', NULL, '$2y$10$PoFoTA/kBrADMdjhYDm0iufnL2ZCdaSQeB2KeUK/XJVzg19fh4kLy', 0, 'inactive', '', NULL, '2021-06-13 08:08:20', '2021-06-13 08:08:20'),
(23, 'Rawan  Khaled', 'rony@gmail.com', NULL, '$2y$10$xoho4kok2R7cqdxofBJmpO1Mu1bV209plGGg9XD0sOMApRlooP6lu', 0, 'inactive', '', NULL, '2021-06-14 07:12:52', '2021-06-14 07:12:52'),
(26, 'ronyyyy', 'rony13@outlook.sa', NULL, '$2y$10$eIoIsqP1DpYWqEz9XmH0vu1VBC/hoaTEI6qHM44iv7mc.5lvpDTXm', 0, 'active', 'system_user', NULL, '2021-06-15 10:03:32', '2021-06-20 12:43:39'),
(28, 'Trader22', 'trader22@gmail.com', NULL, '$2y$10$9ZowDEjyvLHZ2TuLztxILu0TBngxJUUQggWAcRxOBE0i84dq3veBC', 3, 'active', 'trader', NULL, '2021-06-16 06:45:19', '2021-06-23 11:57:21'),
(29, 'test_trader', 'test_trader@gmail.com', NULL, '$2y$10$5zDCT5SYNNQIgDA/lrnEIeXpk8meryAuUXM3oMNh8c0PdhvLiuojy', 5, 'active', 'trader', NULL, '2021-06-16 10:20:27', '2021-06-16 10:20:27'),
(30, 'test_fctory', 'test_factory@gmail.com', NULL, '$2y$10$14ZEgWpp.KaGO8acHOaRZuHwgAbuV8tdxogdqAOUO2QKg5M7Lhudi', 5, 'active', 'factory', NULL, '2021-06-16 18:03:32', '2021-06-23 11:10:41'),
(31, 'test_quarry', 'test_quary@gmai.com', NULL, '$2y$10$04lmey0ZkgQCtTOpv0HgjuGxU39OfGkP/aUWEPc4Na6.s4GnEBV32', 5, 'active', 'quarry', NULL, '2021-06-16 23:05:53', '2021-06-16 23:05:53'),
(33, 'test_fctory2', 'test_factory2@gmail.com', NULL, '$2y$10$MwZaG3EyTmIofvqpNovORuYRCfcAvkUSiW97/uur7cvKkfiUX7SYe', 8, 'active', 'factory', NULL, '2021-06-17 12:15:31', '2021-06-17 12:15:31'),
(34, 'test_quarry', 'test_quarry2@gmail.com', NULL, '$2y$10$tpoY6WfleFswayw1ir125eUMlrB5GZEsDdJd2y2B03Um2PV3sAnH2', 8, 'active', 'quarry', NULL, '2021-06-17 12:16:45', '2021-06-17 12:16:45'),
(35, 'test Trader', 'test_trader2@gmail.com', NULL, '$2y$10$cVF4fpEAs2319nc4NaLUquho14te5UvGIcli9tCll3bejDkasqc2e', 8, 'active', 'trader', NULL, '2021-06-17 12:17:49', '2021-06-17 12:17:49'),
(36, 'Factory 3', 'test_factory3@gmail.com', NULL, '$2y$10$UduLSgxdHafAjN7NzcN9Du8WUu7G.5usiJXrtlxOuN/1LQ.5CGFG2', 9, 'active', 'factory', NULL, '2021-06-21 08:09:22', '2021-06-23 11:00:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sectors`
--
ALTER TABLE `sectors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `sectors`
--
ALTER TABLE `sectors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
